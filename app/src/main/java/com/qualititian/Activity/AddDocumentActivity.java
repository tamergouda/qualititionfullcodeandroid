package com.qualititian.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qualititian.Model.CommonModel;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.FilePath;
import com.qualititian.Utility.ParamEnum;
import com.qualititian.Utility.TakeImage;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class AddDocumentActivity extends AppCompatActivity implements com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener {

    private static final int PDF_REQ_CODE = 222;
    private static final int PICKFILE_RESULT_CODE = 223;
    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;


    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.edName)
    EditText edName;

    @BindView(R.id.edNoOfPages)
    EditText edNoOfPages;

    @BindView(R.id.edDepartment)
    EditText edDepartment;

    @BindView(R.id.edPolicyType)
    EditText edPolicyType;

    @BindView(R.id.edPolicyTitle)
    EditText edPolicyTitle;

    @BindView(R.id.edPolicyNo)
    EditText edPolicyNo;


    @BindView(R.id.edReviewDate)
    EditText edReviewDate;

    @BindView(R.id.edeffectiveDate)
    EditText edeffectiveDate;

    @BindView(R.id.edReplaceDate)
    EditText edReplaceDate;

    @BindView(R.id.edUpdateDate)
    EditText edUpdateDate;

    @BindView(R.id.edPurpose)
    EditText edPurpose;

    @BindView(R.id.edScope)
    EditText edScope;

    @BindView(R.id.edDefination)
    EditText edDefination;

    @BindView(R.id.edPolicy)
    EditText edPolicy;

    @BindView(R.id.edProcedure)
    EditText edProcedure;

    @BindView(R.id.edAttatchments)
    EditText edAttatchments;

    @BindView(R.id.edReferences)
    EditText edReferences;

    @BindView(R.id.edApprovals)
    EditText edApprovals;

     @BindView(R.id.edPreparedBy)
     EditText edPreparedBy;

     @BindView(R.id.edReviewBy)
     EditText edReviewBy;

    @BindView(R.id.edAccerdiation)
    EditText edAccerdiation;

    @BindView(R.id.edChapterNumber)
    EditText edChapterNumber;

    @BindView(R.id.edStandardNumber)
    EditText edStandardNumber;

    @BindView(R.id.edPolicayCategory)
    EditText edPolicayCategory;


    @BindView(R.id.ivSort)
    ImageView ivSort;

    @BindView(R.id.ivThumberImage)
    ImageView ivThumberImage;

    @BindView(R.id.ivAttachDoc)
    ImageView ivAttachDoc;

    private String imagePath = "";

    private String day = "";
    private String month = "";
    private String years = "";

    private int editTextNo;

    private static final int CAMERA_REQUEST = 11;
    private static final int GALLERY_REQUEST = 1;
    private String filePath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.adddoc_layout);
        ButterKnife.bind(this);


        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        ivSort.setVisibility(View.GONE);
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText("Add Documents");


        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        //edDob.setText(simpleDateFormat.format(calendar.getTime()));


        String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1)
                + "/" + String.valueOf(year);

        day = String.valueOf(dayOfMonth);
        month = String.valueOf(monthOfYear + 1);
        years = String.valueOf(year);

        switch (editTextNo) {

            case 1:
                edReviewDate.setText(date);
                break;

            case 2:
                edeffectiveDate.setText(date);
                break;

            case 3:
                edReplaceDate.setText(date);
                break;

            case 4:
                edUpdateDate.setText(date);
                break;

        }


    }

    @OnClick({R.id.edReviewDate, R.id.edeffectiveDate, R.id.edReplaceDate,
            R.id.edUpdateDate, R.id.ivThumberImage, R.id.btnSubmit, R.id.ivAttachDoc})
    void onClick(View view) {

        switch (view.getId()) {


            case R.id.edReviewDate:

                editTextNo = 1;

                if (!day.equalsIgnoreCase("") || !month.equalsIgnoreCase("") || !years.equalsIgnoreCase(""))
                    showDate(Integer.parseInt(years), Integer.parseInt(month), Integer.parseInt(day), R.style.NumberPickerStyle);
                else
                    showDate(1991, 0, 1, R.style.NumberPickerStyle);

                break;


            case R.id.edeffectiveDate:

                editTextNo = 2;

                if (!day.equalsIgnoreCase("") || !month.equalsIgnoreCase("") || !years.equalsIgnoreCase(""))
                    showDate(Integer.parseInt(years), Integer.parseInt(month), Integer.parseInt(day), R.style.NumberPickerStyle);
                else
                    showDate(1991, 0, 1, R.style.NumberPickerStyle);

                break;

            case R.id.edReplaceDate:

                editTextNo = 3;

                if (!day.equalsIgnoreCase("") || !month.equalsIgnoreCase("") || !years.equalsIgnoreCase(""))
                    showDate(Integer.parseInt(years), Integer.parseInt(month), Integer.parseInt(day), R.style.NumberPickerStyle);
                else
                    showDate(1991, 0, 1, R.style.NumberPickerStyle);

                break;

            case R.id.edUpdateDate:

                editTextNo = 4;

                if (!day.equalsIgnoreCase("") || !month.equalsIgnoreCase("") || !years.equalsIgnoreCase(""))
                    showDate(Integer.parseInt(years), Integer.parseInt(month), Integer.parseInt(day), R.style.NumberPickerStyle);
                else
                    showDate(1991, 0, 1, R.style.NumberPickerStyle);

                break;

            case R.id.ivThumberImage:

                openBottomSheetBanner();


                break;

            case R.id.btnSubmit:

                addDocument();

                break;

            case R.id.ivAttachDoc:


                Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT);
                pickIntent.setType("*/*");
                startActivityForResult(pickIntent,PICKFILE_RESULT_CODE);


                break;

        }

    }

    void showDate(int dayOfMonth, int monthOfYear, int year, int spinnerTheme) {
        new SpinnerDatePickerDialogBuilder()
                .context(AddDocumentActivity.this)
                .callback(AddDocumentActivity.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(dayOfMonth, monthOfYear, year)
                .build()
                .show();
    }


    private void openBottomSheetBanner() {

        View view = getLayoutInflater().inflate(R.layout.bottomsheet_imagepicker, null);

        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();


        view.findViewById(R.id.cameraView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AddDocumentActivity.this, TakeImage.class);
                intent.putExtra(ParamEnum.FROM.theValue(), "camera");
                startActivityForResult(intent, CAMERA_REQUEST);
                mBottomSheetDialog.dismiss();
            }
        });

        view.findViewById(R.id.galleryView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddDocumentActivity.this, TakeImage.class);
                intent.putExtra(ParamEnum.FROM.theValue(), "gallery");
                startActivityForResult(intent, GALLERY_REQUEST);
                mBottomSheetDialog.dismiss();
            }
        });


        view.findViewById(R.id.cancelView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){

            case CAMERA_REQUEST:


                if (resultCode == this.RESULT_OK) {
                    imagePath = data.getStringExtra("filePath");
                    if (imagePath != null) {
                        File imgFile = new File(data.getStringExtra("filePath"));
                        if (imgFile.exists()) {
                            ivThumberImage.setImageURI(Uri.fromFile(imgFile));
                        }
                    }

                }


            break;


            case GALLERY_REQUEST:


                if (resultCode == this.RESULT_OK) {
                    imagePath = data.getStringExtra("filePath");
                    if (imagePath != null) {
                        File imgFile = new File(data.getStringExtra("filePath"));
                        if (imgFile.exists()) {
                            ivThumberImage.setImageURI(Uri.fromFile(imgFile));
                        }
                    }

                }

                break;


                case PICKFILE_RESULT_CODE:


                if (resultCode == this.RESULT_OK) {

                  Uri   uri = data.getData();

                filePath= FilePath.getPath(this,uri);

                  Log.w("this is file path",filePath);


                }

                break;


        }

    }



    ////////////////data mapping////////////////////
    private Map<String, RequestBody> setUpMapData() {

        Map<String, RequestBody> map = new HashMap<>();
        map.put("token", RequestBody.create(MediaType.parse("text/plain"), SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN)));
        map.put("hospital_name", RequestBody.create(MediaType.parse("text/plain"), edName.getText().toString()));
        map.put("department", RequestBody.create(MediaType.parse("text/plain"), edDepartment.getText().toString()));
        map.put("no_pages", RequestBody.create(MediaType.parse("text/plain"), edNoOfPages.getText().toString()));
        map.put("policy_number", RequestBody.create(MediaType.parse("text/plain"), edPolicyNo.getText().toString()));
        map.put("policy_category", RequestBody.create(MediaType.parse("text/plain"), edPolicayCategory.getText().toString()));
        map.put("policy_title", RequestBody.create(MediaType.parse("text/plain"), edPolicyTitle.getText().toString()));
        map.put("policy_type", RequestBody.create(MediaType.parse("text/plain"), edPolicyType.getText().toString()));
        map.put("effective_date", RequestBody.create(MediaType.parse("text/plain"),edeffectiveDate.getText().toString()));
        map.put("review_date", RequestBody.create(MediaType.parse("text/plain"), edReviewDate.getText().toString()));
        map.put("replace_date", RequestBody.create(MediaType.parse("text/plain"), edReplaceDate.getText().toString()));
        map.put("update_date", RequestBody.create(MediaType.parse("text/plain"), edUpdateDate.getText().toString()));
        map.put("purpose", RequestBody.create(MediaType.parse("text/plain"), edPurpose.getText().toString()));
        map.put("scope", RequestBody.create(MediaType.parse("text/plain"), edScope.getText().toString()));
        map.put("definations", RequestBody.create(MediaType.parse("text/plain"),  edDefination.getText().toString()));
        map.put("policy", RequestBody.create(MediaType.parse("text/plain"), edPolicy.getText().toString()));
        map.put("attechments", RequestBody.create(MediaType.parse("text/plain"), edAttatchments.getText().toString()));
        map.put("reference", RequestBody.create(MediaType.parse("text/plain"), edReferences.getText().toString()));
        map.put("approvle", RequestBody.create(MediaType.parse("text/plain"), edApprovals.getText().toString()));
        map.put("prepared_by", RequestBody.create(MediaType.parse("text/plain"), edPreparedBy.getText().toString()));
        map.put("reviewed_by", RequestBody.create(MediaType.parse("text/plain"), edReviewBy.getText().toString()));
        map.put("standard_no", RequestBody.create(MediaType.parse("text/plain"), edStandardNumber.getText().toString()));
        map.put("chapter_no", RequestBody.create(MediaType.parse("text/plain"), edChapterNumber.getText().toString()));
        map.put("accrediation", RequestBody.create(MediaType.parse("text/plain"), edAccerdiation.getText().toString()));
        map.put("procedures", RequestBody.create(MediaType.parse("text/plain"),edProcedure.getText().toString()));


        return map;
    }


    private void addDocument() {
        try {
            CommonUtilities.showLoadingDialog(this);
            RequestBody profile_body;
            MultipartBody.Part profilePart;

            if (!imagePath.isEmpty()) {

                File file = new File(imagePath);
                profile_body = RequestBody.create(MediaType.parse("image/*"), file);
                profilePart = MultipartBody.Part.createFormData("hospital_logo", file.getName(), profile_body);
            } else {
                profilePart = MultipartBody.Part.createFormData("hospital_logo", "");
            }

            ServicesInterface apiInterface = ServicesConnection.getInstance().createService();

            Call<CommonModel> call = apiInterface.addDocument(setUpMapData(), file(), profilePart);
            call.enqueue(new retrofit2.Callback<CommonModel>() {
                @Override
                public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                    if (CommonUtilities.customProgressBar != null) {
                        CommonUtilities.dismissLoadingDialog();
                    }
                    if (response.isSuccessful()) {
                        CommonModel bean = response.body();
                        if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {


                            Toast.makeText(AddDocumentActivity.this, "Document add succesfully", Toast.LENGTH_SHORT).show();

                        } else {

                            CommonUtilities.snackBar(AddDocumentActivity.this,bean.getMessage());

                        }

                    } else {
                        Toast.makeText(AddDocumentActivity.this, "Error!", Toast.LENGTH_SHORT).show();

                        if (CommonUtilities.customProgressBar != null) {
                            CommonUtilities.dismissLoadingDialog();
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonModel> call, Throwable t) {
                    t.printStackTrace();
                    if (CommonUtilities.customProgressBar != null) {
                        CommonUtilities.dismissLoadingDialog();
                    }
                }

            });


        } catch (Exception e) {
            e.printStackTrace();

            if (CommonUtilities.customProgressBar != null) {
                CommonUtilities.dismissLoadingDialog();
            }
        }
    }


    private MultipartBody.Part file(){

        RequestBody profile_body;
        MultipartBody.Part fllePart;

        if (!filePath.isEmpty()) {

            File file = new File(filePath);
            profile_body = RequestBody.create(MediaType.parse("file/*"), file);
            fllePart = MultipartBody.Part.createFormData("file", file.getName(), profile_body);
        } else {
            fllePart = MultipartBody.Part.createFormData("file", "");
        }

        return fllePart;
    }

}
