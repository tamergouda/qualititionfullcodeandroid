package com.qualititian.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import com.qualititian.R;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.ParamEnum;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Analytics extends AppCompatActivity {

    @BindView(R.id.notification_navbtn)ImageView notification_navbtn;
    @BindView(R.id.cvPublishDoc)CardView cvPublishDoc;
    @BindView(R.id.cvPendingDoc)CardView cvPendingDoc;
    @BindView(R.id.cvUplaodDoc)CardView cvUplaodDoc;
    @BindView(R.id.cvDownlaodedDoc)CardView cvDownlaodedDoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.notification_navbtn,R.id.cvDownlaodedDoc,R.id.cvPublishDoc,R.id.cvPendingDoc,R.id.cvUplaodDoc})
    void onCLick(View view){
        Intent intent;
        switch (view.getId()){

            case R.id.notification_navbtn:

                finish();

                break;

                case R.id.cvPublishDoc:

                    intent = new Intent(this, WebViewChromeClient.class);
                    intent.putExtra("URL", "http://mobulous.co.in/qualitian/services/approvedDocument1/"+ SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID));
                    intent.putExtra(ParamEnum.Title.theValue(), "Publish Documents");
                    startActivity(intent);

                break;

            case R.id.cvPendingDoc:

                intent = new Intent(this, WebViewChromeClient.class);
                intent.putExtra("URL", "http://mobulous.co.in/qualitian/services/pendingDocument1/"+SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID));
                intent.putExtra(ParamEnum.Title.theValue(), "Pending Documents");
                startActivity(intent);


                break;

                case R.id.cvUplaodDoc:

                intent = new Intent(this, UplaodedDocuments.class);
                startActivity(intent);


                break;

                case R.id.cvDownlaodedDoc:

                intent = new Intent(this, DownloadedDoc.class);
                startActivity(intent);


                break;

        }

    }
}
