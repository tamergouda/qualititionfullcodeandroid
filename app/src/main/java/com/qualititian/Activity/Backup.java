package com.qualititian.Activity;

import android.content.Intent;
import android.os.DropBoxManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dropbox.core.android.Auth;
import com.dropbox.core.v2.users.FullAccount;
import com.qualititian.DropBox.DropboxClient;
import com.qualititian.DropBox.URI_to_Path;
import com.qualititian.DropBox.UploadTask;
import com.qualititian.DropBox.UserAccountTask;
import com.qualititian.R;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Backup extends AppCompatActivity {

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;
    private static final int IMAGE_REQUEST_CODE = 27;

    private String accessTokens = "";

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.tvBrowseFileDropBox)
    TextView tvBrowseFileDropBox;

    @BindView(R.id.tvSignOutDropBox)
    TextView tvSignOutDropBox;

    @BindView(R.id.clGoogleDrive)
    ConstraintLayout clGoogleDrive;

    @BindView(R.id.clDropbox)
    ConstraintLayout clDropbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);
        ButterKnife.bind(this);

        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);

        hideUi();

        tv_title.setText("Backup");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @OnClick({R.id.clGoogleDrive, R.id.clDropbox,R.id.tvBrowseFileDropBox,R.id.tvSignOutDropBox})
    void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.clGoogleDrive:

                intent = new Intent(this, UploadToGoogleDrive.class);
                startActivity(intent);

                break;

            case R.id.clDropbox:

                Auth.startOAuth2Authentication(getApplicationContext(), getString(R.string.APP_KEY));



                break;

            case R.id.tvBrowseFileDropBox:

                selectFileToUpload();

                break;

                case R.id.tvSignOutDropBox:

                    hideUi();
                    SharedPreferenceWriter.getInstance(this).writeStringValue(SPreferenceKey.DROPBOX_TOKEN,"");

                break;


        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        getAccessToken();
    }

    public void getAccessToken() {
        String accessToken = Auth.getOAuth2Token(); //generate Access Token
        if (accessToken != null) {
            //Store accessToken in SharedPreferences
            SharedPreferenceWriter.getInstance(Backup.this).writeStringValue(SPreferenceKey.DROPBOX_TOKEN, accessToken);

                /*Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);*/

            retrieveAccessToken();
        }
    }


    ///////retrive acces token for dropBox //////////

    private String retrieveAccessToken() {
        String accessToken = SharedPreferenceWriter.getInstance(Backup.this).getString(SPreferenceKey.DROPBOX_TOKEN);
        if (accessToken == null) {
            Log.d("AccessToken Status", "No token found");
            return null;
        } else {
            //accessToken already exists
            Log.d("AccessToken Status", "Token exists");

            //Get Token
            accessTokens = accessToken;

            getUserAccount();
            return accessToken;
        }
    }

    private void getUserAccount() {

        if (accessTokens == null) return;

        new UserAccountTask(DropboxClient.getClient(accessTokens), new UserAccountTask.AccountDetailListener() {
            @Override
            public void onAccountReceived(FullAccount account) {
                //Get account's detail
                Log.d("User", account.getEmail());
                Log.d("User", account.getName().getDisplayName());
                Log.d("User", account.getAccountType().name());
                updateUI();
            }

            @Override
            public void onError(Exception error) {
                Log.d("User", "Error receiving account details.");
            }
        }).execute();

    }

    private void updateUI() {

        tvBrowseFileDropBox.setVisibility(View.VISIBLE);
        tvSignOutDropBox.setVisibility(View.VISIBLE);


    }

    private void hideUi() {

        tvBrowseFileDropBox.setVisibility(View.GONE);
        tvSignOutDropBox.setVisibility(View.GONE);


    }


    private void selectFileToUpload() {
        if (accessTokens == null) return;

//        Intent intent = new Intent();
//        intent.setType("*/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
//        startActivityForResult(Intent.createChooser(intent,
//                "Upload to Dropbox"), IMAGE_REQUEST_CODE);


        String[] mimeTypes =
                {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "application/zip"};
        Intent intent = new Intent();
        intent.setType("application/* | text/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent,
                "Upload to Dropbox"), IMAGE_REQUEST_CODE);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null) return;
        if (requestCode == IMAGE_REQUEST_CODE) {
            //Image URI received
            File file = new File(URI_to_Path.getPath(getApplication(), data.getData()));
            if (file != null) {
                //Initialize UploadTask
                new UploadTask(DropboxClient.getClient(accessTokens), file, getApplicationContext()).execute();
            }
        }
    }

}
