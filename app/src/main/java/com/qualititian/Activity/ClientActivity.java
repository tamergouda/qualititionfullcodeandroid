package com.qualititian.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.Fonts;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.qualititian.Utility.CommonUtilities.path;

public class ClientActivity extends AppCompatActivity {

    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.signup_login)
    TextView signup_login;
    @BindView(R.id.edCountry)
    EditText edCountry;
    @BindView(R.id.edCode)
    EditText edCode;
    @BindView(R.id.edCorporateName)
    EditText edCorporateName;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.edTypeCorporate)
    EditText edTypeCorporate;
    @BindView(R.id.edDesination)
    EditText edDesination;
    @BindView(R.id.edPhone)
    EditText edPhone;
    @BindView(R.id.edPass)
    EditText edPass;
    @BindView(R.id.edConfirmPass)
    EditText edConfirmPass;
    @BindView(R.id.role_spin)
    Spinner role_spin;

    private String roleId="";
    private String MobilePattern = "[0-9]{10}";
    private ArrayList<String> role;
    private ArrayAdapter<String> arrayAdapter;
    private String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
        ButterKnife.bind(this);

        setRoleDropDown();
    }

    @OnClick({R.id.signup_login, R.id.btnSignUp, R.id.edCountry})
    void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnSignUp:


                if(validationClient())
                    signUp();

                break;


            case R.id.signup_login:

                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);

                break;

            case R.id.edCountry:

                final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        edCountry.setText(name);
                        /*  mCountry = name;*/

                        edCode.setText(dialCode);
                        picker.dismiss();
                    }
                });
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");

                break;
        }

    }


    ////client signUp///////////

    public void signUp() {

        try {

            switch (role_spin.getSelectedItem().toString()){

                case "Owner":
                    roleId="1";
                    break;

                case "Director":
                    roleId="2";
                    break;

                case "Manager":
                    roleId="3";
                    break;

                case "Specialist":
                    roleId="4";
                    break;


                case "Champion":
                    roleId="5";
                    break;

                case "Achiever":
                    roleId="6";
                    break;

                case "Team":
                    roleId="7";
                    break;

            }


            Map<String, RequestBody> map = new HashMap<>();
            map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), edCorporateName.getText().toString()));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), edEmail.getText().toString()));
            map.put("country_code", RequestBody.create(MediaType.parse("text/plain"), edCode.getText().toString()));
            map.put("mobile", RequestBody.create(MediaType.parse("text/plain"), edPhone.getText().toString()));
            map.put("designation", RequestBody.create(MediaType.parse("text/plain"), edDesination.getText().toString()));
            map.put("type_corprate", RequestBody.create(MediaType.parse("text/plain"), edTypeCorporate.getText().toString()));
            map.put("role", RequestBody.create(MediaType.parse("text/plain"),roleId));
            map.put("country", RequestBody.create(MediaType.parse("text/plain"), edCountry.getText().toString()));

            //////////// type user 52 for guest//////////
            map.put("type_user", RequestBody.create(MediaType.parse("text/plain"), "52"));
            map.put("password", RequestBody.create(MediaType.parse("text/plain"), edPass.getText().toString()));
            map.put("confirm_password", RequestBody.create(MediaType.parse("text/plain"), edConfirmPass.getText().toString()));
            map.put("devicetype", RequestBody.create(MediaType.parse("text/plain"), "Android"));
            map.put("devicetoken", RequestBody.create(MediaType.parse("text/plain"), "97975454"));

            ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
            Call<CommonModel> editProfileCall;

            if (path.isEmpty()) {
                editProfileCall = servicesInterface.signUpClient(map);
            } else {
                File imagepath = new File(path);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image", imagepath.getName(), RequestBody.create(MediaType.parse("image/*"), imagepath));
                editProfileCall = servicesInterface.signUpClient(map, body);
            }

            ServicesConnection.getInstance().enqueueWithoutRetry(
                    editProfileCall,
                    this,
                    true,
                    new Callback<CommonModel>() {
                        @Override
                        public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                            if (response.isSuccessful()) {
                                CommonModel bean = ((CommonModel) response.body());
                                if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {

                                    CommonResponse commonResponse = bean.getClientSignup();

                                    SharedPreferenceWriter.getInstance(ClientActivity.this).writeStringValue(SPreferenceKey.ISLOGIN, "Login");
                                    SharedPreferenceWriter.getInstance(ClientActivity.this).writeStringValue(SPreferenceKey.NAME, commonResponse.getFirst_name());
                                    SharedPreferenceWriter.getInstance(ClientActivity.this).writeStringValue(SPreferenceKey.USERID, commonResponse.getId());

                                    SharedPreferenceWriter.getInstance(ClientActivity.this).writeStringValue(SPreferenceKey.USER_TYPE, commonResponse.getType_user());
                                    SharedPreferenceWriter.getInstance(ClientActivity.this).writeStringValue(SPreferenceKey.ROLE, commonResponse.getRole());

                                    Intent intent = new Intent(ClientActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finishAffinity();

                                    Toast.makeText(ClientActivity.this, "Account Created Succesfuly", Toast.LENGTH_SHORT).show();


                                } else {
                                    CommonUtilities.snackBar(ClientActivity.this, bean.getMessage());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<CommonModel> call, Throwable t) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setRoleDropDown() {
        // final Spinner spinner =sprole;

        role = new ArrayList<>();
        role.add("Role");
        role.add("Owner");
        role.add("Director");
        role.add("Manager");
        role.add("Specialist");
        role.add("Champion");
        role.add("Achiever");
        role.add("Team");


        List<String> spinnerList = role;

        arrayAdapter = new ArrayAdapter<String>(ClientActivity.this, R.layout.spinner_layout, spinnerList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    //Disable the first item of spinner.
                    // Log.i(TAG, "Position[0]: Spinner first item is disabled");

                    return false;
                } else {
                    String itemSelected = role_spin.getItemAtPosition(position).toString();
                    // Log.i(TAG, "Selected Item[" + position + "]: " + itemSelected);
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View spinnerView = super.getDropDownView(position, convertView, parent);

                TextView spinnerTextV = (TextView) spinnerView;
                if (position == 0) {
                    //Set the disable spinner item color fade .
                    spinnerTextV.setTextColor(Color.parseColor("#a7a7a6"));
                } else {
                    spinnerTextV.setTextColor(Color.parseColor("#b2000000"));
                }
                return spinnerView;
            }
        };

        arrayAdapter.setDropDownViewResource(R.layout.spinner_layout);

        role_spin.setAdapter(arrayAdapter);

        /* //      //Set Spinner OnItemSelectedListener*/
        AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String user_type = parent.getItemAtPosition(position).toString();

                Log.d("user_typefirtstime", user_type);


                if (((TextView) parent.getChildAt(0)) != null) {

                    if (user_type.equalsIgnoreCase("Role")) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.grey));
                    } else {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    }
                    ((TextView) parent.getChildAt(0)).setTextSize(20);
                    ((TextView) parent.getChildAt(0)).setPadding(16, 0, 0, 0);
                    Fonts.OpenSans_Regular_Txt(((TextView) parent.getChildAt(0)), ClientActivity.this.getAssets());

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        role_spin.setOnItemSelectedListener(onItemSelectedListener);

    }


    private boolean validationClient() {

        if (edCorporateName.getText().toString().length() > 0) {


            if (edEmail.getText().toString().length() > 0) {

                if (edTypeCorporate.getText().toString().length() > 0) {


                    if (!role_spin.getSelectedItem().toString().equalsIgnoreCase("Role")) {

                        if (edDesination.getText().toString().length() > 0) {

                            if (edCountry.getText().toString().length() > 0) {


                                if (edCode.getText().toString().length() > 0) {


                                    if (edPhone.getText().toString().matches(MobilePattern)) {


                                        if (edPass.getText().toString().length() > 0) {


                                            if (edConfirmPass.getText().toString().length() > 0) {


                                                if (edPass.getText().toString().equalsIgnoreCase(edConfirmPass.getText().toString())) {


                                                    return true;

                                                } else {

                                                    CommonUtilities.snackBar(this, "Enter Confirm Password");


                                                }
                                            } else {

                                                CommonUtilities.snackBar(this, "Enter Confirm Password");


                                            }
                                        } else {

                                            CommonUtilities.snackBar(this, "Enter Password");


                                        }
                                    } else {

                                        CommonUtilities.snackBar(this, "Enter Valid Phone Number");


                                    }
                                } else {

                                    CommonUtilities.snackBar(this, "Select Code");


                                }
                            } else {

                                CommonUtilities.snackBar(this, "Select Country");


                            }
                        } else {

                            CommonUtilities.snackBar(this, "Select Designation");


                        }
                    } else {

                        CommonUtilities.snackBar(this, "Select Role");


                    }
                } else {

                    CommonUtilities.snackBar(this, "Enter Corporate Type");


                }


            } else {

                CommonUtilities.snackBar(this, "Enter Your Email");

            }

        } else {

            CommonUtilities.snackBar(this, "Enter Corporate Name");

        }


        return false;
    }

}
