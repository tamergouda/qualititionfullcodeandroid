package com.qualititian.Activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.qualititian.Adapter.MyViewPagerAdapter;
import com.qualititian.Fragment.Module;
import com.qualititian.Fragment.MyDocuments;
import com.qualititian.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DocumentsModuleTab extends AppCompatActivity implements TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    private Bundle saveInstance;

    private MyViewPagerAdapter myViewPagerAdapter;

    @BindView(R.id.tbMydocuments)
    TabLayout tbMydocuments;

    @BindView(R.id.ivSorting)
    ImageView ivSorting;

    @BindView(R.id.ivDocSearch)
    ImageView ivDocSearch;

    @BindView(R.id.vpMyDocs)

    ViewPager vpMyDocs;
    public int selectedTab = 0;


    private MyDocuments myDocuments;

    private Module module;
    private FragmentManager childFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_documents);
        ButterKnife.bind(this);

        saveInstance = savedInstanceState;


        //        INITIALIZE CHILD FRAGMENTS
        myDocuments = new MyDocuments();
        module = new Module();
        configureViewPager();
        vpMyDocs.addOnPageChangeListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        switch (position){

            case 0:

                setSearchWidgetOnOff(true);

                break;

            case 1:
                setSearchWidgetOnOff(false);

                module.getmoduleListing(DocumentsModuleTab.this);

                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    //    METHOD: TO  Configure ViewPager
    private void configureViewPager() {
        childFragmentManager = getSupportFragmentManager();

        vpMyDocs.setOffscreenPageLimit(2);
        populateViewPager(vpMyDocs);

        tbMydocuments.setupWithViewPager(vpMyDocs);

        //setUpCustomView();

//      SET UP ON TAB SELECTED LISTENER (for Selected Tab Position)
        tbMydocuments.addOnTabSelectedListener(this);


    }

    //  METHOD: TO POPULATE VIEW PAGER WITH FRAGMENTS
    private void populateViewPager(ViewPager viewPager) {

        myViewPagerAdapter = new MyViewPagerAdapter(childFragmentManager);

        myViewPagerAdapter.addNewFragment(myDocuments, "My Documents");
        myViewPagerAdapter.addNewFragment(module, "Module");


        viewPager.setAdapter(myViewPagerAdapter);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveInstance = outState;

        saveInstance.putInt("tabPos", selectedTab);
    }
    //  METHOD: TO SELECT A PARTICULAR TAB BASED ON TAB_POSITION(pageIndex) AND HIT API/SERVICE ..

    public void selectPage(int pageIndex) {

        try {



                switch (pageIndex) {
                    case 0:

                        setSearchWidgetOnOff(true);
                        break;

                    case 1:

                        setSearchWidgetOnOff(false);
                        module.getmoduleListing(DocumentsModuleTab.this);


                        break;

                }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    @OnClick({R.id.ivBack})
    void onClick(View view) {

          switch (view.getId()){


              case R.id.ivBack:

                  finish();

                  break;
          }


    }

    @Override
    public void onResume() {
        super.onResume();

        //      ADD ON PAGE CHANGE LISTER WITH VIEW PAGER

        if (saveInstance != null) {
            selectedTab = saveInstance.getInt("tabPos", 0);

            selectPage(selectedTab);
        } else {
            // Hitting Shop fragment LIST SERVICE when Screen Appear
            //shopFragment.shopService(getActivity(),isGrid, true,datetime);      //  SERVICE HIT..


            selectPage(selectedTab);
        }

    }

    private void setSearchWidgetOnOff(boolean onOff){

        if(onOff){
            ivDocSearch.setVisibility(View.VISIBLE);
            ivSorting.setVisibility(View.VISIBLE);
        }else {
            ivDocSearch.setVisibility(View.GONE);
            ivSorting.setVisibility(View.GONE);
        }

    }
}
