package com.qualititian.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {


    @BindView(R.id.forgot_nextbtn)
    Button forgot_nextbtn;

    @BindView(R.id.edEmailPhone)
    EditText edEmailPhone;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @BindView(R.id.ivback)
    ImageView ivback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.forgot_nextbtn,
            R.id.edEmailPhone,
            R.id.ivback})
    void onCLick(View view) {

        switch (view.getId()) {

            case R.id.forgot_nextbtn:

                if (edEmailPhone.getText().toString().matches(emailPattern))
                    forgotPassword();
                else
                    CommonUtilities.snackBar(this, "Enter Valid Email");

                break;

            case R.id.ivback:

                onBackPressed();

                break;

        }


    }

    public void forgotPassword() {

        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.forgotPassword(edEmailPhone.getText().toString());

                Log.w("DeviceToke", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {

                                        CommonResponse responseBean = bean.getForgotPassword();

                                        Intent intent = new Intent(ForgotPassword.this, OtpScreen.class);
                                        intent.putExtra("OTP", responseBean.getCode());
                                        intent.putExtra("Email", edEmailPhone.getText().toString());
                                        startActivity(intent);

                                    } else {
                                        CommonUtilities.snackBar(ForgotPassword.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(this, getString(R.string.no_internet));

        }
    }

}
