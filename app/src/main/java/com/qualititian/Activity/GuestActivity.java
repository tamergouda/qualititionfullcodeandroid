package com.qualititian.Activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GuestActivity extends AppCompatActivity {


    @BindView(R.id.btnSignUp)
    Button btnSignUp;

    @BindView(R.id.signup_login)
    TextView signup_login;

    @BindView(R.id.edFirst)
    EditText edFirst;

    @BindView(R.id.edLastName)
    EditText edLastName;

    @BindView(R.id.edEmail)
    EditText edEmail;

    @BindView(R.id.cbTerms)
    CheckBox cbTerms;

    @BindView(R.id.edPass)
    EditText edPass;

    @BindView(R.id.edConfirmPass)
    EditText edConfirmPass;

    private String path = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.signup_login, R.id.btnSignUp})
    void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnSignUp:


                if(validation())
                    signUp();


                break;


            case R.id.signup_login:

                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);

                break;
        }

    }


    ////guest signUp///////////

    public void signUp() {
        try {
            Map<String, RequestBody> map = new HashMap<>();
            map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), edFirst.getText().toString()));
            map.put("last_name", RequestBody.create(MediaType.parse("text/plain"), edLastName.getText().toString()));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), edEmail.getText().toString()));

            //////////// type user 51 for guest//////////
            map.put("type_user", RequestBody.create(MediaType.parse("text/plain"), "51"));
            map.put("password", RequestBody.create(MediaType.parse("text/plain"), edPass.getText().toString()));
            map.put("confirm_password", RequestBody.create(MediaType.parse("text/plain"), edConfirmPass.getText().toString()));
            map.put("devicetype", RequestBody.create(MediaType.parse("text/plain"), "Android"));
            map.put("devicetoken", RequestBody.create(MediaType.parse("text/plain"), "2458"));

            ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
            Call<CommonModel> editProfileCall;

            if (path.isEmpty()) {
                editProfileCall = servicesInterface.signUpGuest(map);
            } else {
                File imagepath = new File(path);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image", imagepath.getName(), RequestBody.create(MediaType.parse("image/*"), imagepath));
                editProfileCall = servicesInterface.signUpGuest(map, body);
            }

            ServicesConnection.getInstance().enqueueWithoutRetry(
                    editProfileCall,
                    this,
                    true,
                    new Callback<CommonModel>() {
                        @Override
                        public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                            if (response.isSuccessful()) {
                                CommonModel bean = ((CommonModel) response.body());
                                if (bean.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {

                                     CommonResponse commonResponse = bean.getGuestSignup();

                                    SharedPreferenceWriter.getInstance(GuestActivity.this).writeStringValue(SPreferenceKey.NAME, commonResponse.getFirst_name());
                                    SharedPreferenceWriter.getInstance(GuestActivity.this).writeStringValue(SPreferenceKey.USER_TYPE, commonResponse.getType_user());
                                    SharedPreferenceWriter.getInstance(GuestActivity.this).writeStringValue(SPreferenceKey.ISLOGIN,"Login");
                                    SharedPreferenceWriter.getInstance(GuestActivity.this).writeStringValue(SPreferenceKey.USERID, commonResponse.getId());

                                    Intent intent = new Intent(GuestActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finishAffinity();

                                    Toast.makeText(GuestActivity.this, "Account Created Succesfuly", Toast.LENGTH_SHORT).show();


                                } else {
                                    CommonUtilities.snackBar(GuestActivity.this, bean.getMessage());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<CommonModel> call, Throwable t) {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean validation() {


        if (edFirst.getText().toString().length() > 0) {

            if (edLastName.getText().toString().length() > 0) {

                if (edEmail.getText().toString().length() > 0) {

                    if (edPass.getText().toString().length() > 0) {

                        if (edConfirmPass.getText().toString().length() > 0) {

                            if (edPass.getText().toString().equalsIgnoreCase(edConfirmPass.getText().toString())) {

                                if(cbTerms.isChecked()){

                                return true;

                            }else{

                                    CommonUtilities.snackBar(this, "Please Accept Terms & Conditions");


                                }
                            } else {

                                CommonUtilities.snackBar(this, "Password and Confirm Password do not match");


                            }


                        } else {

                            CommonUtilities.snackBar(this, "Enter Confirm Password");


                        }

                    } else {

                        CommonUtilities.snackBar(this, "Enter Password");

                    }

                } else {

                    CommonUtilities.snackBar(this, "Enter Email Id");
                }

            } else {

                CommonUtilities.snackBar(this, "Enter Last Name");
            }

        } else {

            CommonUtilities.snackBar(this, "Enter First Name");
        }


        return false;
    }

}
