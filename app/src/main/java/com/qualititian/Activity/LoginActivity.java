package com.qualititian.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.login_btn)
    Button login_btn;

    @BindView(R.id.tvSignup)
    TextView tvSignup;

    @BindView(R.id.login_forgotpass)
    TextView login_forgotpass;

    @BindView(R.id.login_email)
    EditText login_email;

    @BindView(R.id.edLoginPass)
    EditText edLoginPass;

    @BindView(R.id.login_checkbox)
    CheckBox login_checkbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        checkIsRember();
        setUpBlueTextWithUnderLine(tvSignup, 24, 31, "Don't have an account ? Sign Up");

        login_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked){

                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.ISREMEBER,"IsLogin");
                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.LOGIN_EMAIL_PHONE, login_email.getText().toString());
                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.PASSWORD, edLoginPass.getText().toString());

                }else {

                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.ISREMEBER,"IsNotLogin");
                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.LOGIN_EMAIL_PHONE, "");
                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.PASSWORD, "");

                }

            }
        });

        //  getDeviceToken();
    }


    @OnClick({R.id.login_btn, R.id.tvSignup, R.id.login_forgotpass})
    void onClick(View view) {

        Intent intent;

        switch (view.getId()) {

            case R.id.login_btn:

                if (validation())
                    loginServiceHit();

                break;


            case R.id.tvSignup:

                intent = new Intent(this, SplashLoginType.class);
                startActivity(intent);

                break;

            case R.id.login_forgotpass:

                intent = new Intent(this, ForgotPassword.class);
                startActivity(intent);

                break;

        }

    }

    //    METHOD: TO DRAW UNDERLINE BELOW TEXT_VIEW AND MAKE TEXT AND LINE IN BLUE COLOR

    private void setUpBlueTextWithUnderLine(TextView textView, int start, int end, String str) {

        SpannableString content = new SpannableString(str);

        content.setSpan(new ForegroundColorSpan(Color.parseColor("#3DCFD5")), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

//        content.setSpan(new StyleSpan(Typeface.ITALIC), start, end, 0);

        textView.setText(content);
    }


    //////////login api///////

    public void loginServiceHit() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.login(login_email.getText().toString(), "Android", "874565", edLoginPass.getText().toString());

                Log.w("DeviceToken", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {

                                        CommonResponse responseBean = bean.getLoginResponse();

                                        Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();

                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.TOKEN, responseBean.getToken());
                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.USERID, responseBean.getId());
                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.USER_TYPE, responseBean.getType_user());
                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.ROLE, responseBean.getRole());
                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.USER_TYPE, responseBean.getType_user());
                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.NAME, responseBean.getFirst_name());

                                        if (login_checkbox.isChecked()) {

                                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.PASSWORD, edLoginPass.getText().toString());
                                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.LOGIN_EMAIL_PHONE, login_email.getText().toString());
                                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.ISLOGIN, "Login");

                                        } else {

                                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.PASSWORD, "");
                                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.LOGIN_EMAIL_PHONE, "");
                                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.ISLOGIN, "Logout");


                                        }


                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                    } else {
                                        CommonUtilities.snackBar(LoginActivity.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(this, getString(R.string.no_internet));

        }
    }


    private boolean validation() {

        if (login_email.getText().toString().length() > 0) {

            if (edLoginPass.getText().toString().length() > 0) {

                return true;
            } else {
                CommonUtilities.snackBar(this, "Enter Password");
            }
        } else {
            CommonUtilities.snackBar(this, "Enter Phone No/Email Id");
        }
        return false;
    }

    private void getDeviceToken() {

        final Thread thread = new Thread() {
            @Override
            public void run() {
                Log.e(">>>>>>>>>>>>>>", "thred IS  running");
                try {

                    SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.DEVICETOKEN, "");

                    if (SharedPreferenceWriter.getInstance(LoginActivity.this).getString(SPreferenceKey.DEVICETOKEN).isEmpty()) {
                        String token = FirebaseInstanceId.getInstance().getToken();
//                        String token = Settings.Secure.getString(getApplicationContext().getContentResolver(),
//                                Settings.Secure.ANDROID_ID);
                        Log.e("Generated Device Token", "-->" + token);
                        if (token == null) {
                            getDeviceToken();
                        } else {

                            SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.DEVICETOKEN, token);
                            //mPreference.writeStringValue(SharedPreferenceKey.DEVICE_TOKEN, token);
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                super.run();
            }
        };
        thread.start();

    }


    private void checkIsRember(){
        if(SharedPreferenceWriter.getInstance(LoginActivity.this).getString(SPreferenceKey.ISREMEBER).equalsIgnoreCase("IsLogin")){
            login_checkbox.setChecked(true);
            login_email.setText(SharedPreferenceWriter.getInstance(LoginActivity.this).getString(SPreferenceKey.LOGIN_EMAIL_PHONE));
            edLoginPass.setText(SharedPreferenceWriter.getInstance(LoginActivity.this).getString(SPreferenceKey.PASSWORD));

        }else {
            login_checkbox.setChecked(false);
            login_email.setText("");
            edLoginPass.setText("");
        }
    }

}
