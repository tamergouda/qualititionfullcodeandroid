package com.qualititian.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qualititian.Fragment.HomeFragment;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private HomeFragment homeFragment = new HomeFragment();

    @BindView(R.id.toolbarImg)
    ImageView toolbarImg;


    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    LinearLayout navigatin;
    LinearLayout contactUsLay;
    LinearLayout aboutUsLay;
    LinearLayout logoutLay;
    TextView userNameTxtSide;

    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        navigatin = (LinearLayout) header.findViewById(R.id.notificationLay);
        aboutUsLay = (LinearLayout) header.findViewById(R.id.aboutUsLay);
        contactUsLay = (LinearLayout) header.findViewById(R.id.contactUsLay);
        logoutLay = (LinearLayout) header.findViewById(R.id.logoutLay);
        userNameTxtSide = (TextView) header.findViewById(R.id.userNameTxtSide);


        userNameTxtSide.setText(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.NAME));

        navigatin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, NotificationScreen.class);
                startActivity(intent);
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });


        logoutLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutService();
                drawer_layout.closeDrawer(GravityCompat.START);


            }
        });

        aboutUsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchAboutUs();
                drawer_layout.closeDrawer(GravityCompat.START);


            }
        });

        contactUsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchContactUs();
                drawer_layout.closeDrawer(GravityCompat.START);


            }
        });

        toggle = new ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer_layout.setDrawerListener(toggle);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, homeFragment).commit();


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }


    private void openHomeNavigationDrawer() {
        if (!drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.openDrawer(GravityCompat.START);
        }
    }


    @OnClick({R.id.toolbarImg})
    void setClick(View view) {

        Intent intent;
        switch (view.getId()) {

            case R.id.toolbarImg:

                openHomeNavigationDrawer();

                break;


        }
    }

    //////logout service/////////
    public void logoutService() {

        if(CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.logout(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));

                Log.w("DeviceToke", SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {

                                        CommonResponse responseBean = bean.getLoginResponse();


                                        SharedPreferenceWriter.getInstance(MainActivity.this).writeStringValue(SPreferenceKey.TOKEN, "");
                                        SharedPreferenceWriter.getInstance(MainActivity.this).writeStringValue(SPreferenceKey.USER_TYPE, "");
                                        SharedPreferenceWriter.getInstance(MainActivity.this).writeStringValue(SPreferenceKey.ISLOGIN, "Logout");

                                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();

                                        Toast.makeText(MainActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();



                                    } else {
                                        CommonUtilities.snackBar(MainActivity.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            CommonUtilities.snackBar(this,getString(R.string.no_internet));

        }
    }

    private void dispatchContactUs(){

        Intent intent = new Intent(this, PotraitChromClient.class);
        intent.putExtra("URL", "http://mobulous.co.in/qualitian/services/contact");
        intent.putExtra("TITLE", "Contact Us");
        startActivity(intent);

    }

    private void dispatchAboutUs(){

        Intent intent = new Intent(this, PotraitChromClient.class);
        intent.putExtra("URL", "http://mobulous.co.in/qualitian/services/about");
        intent.putExtra("TITLE", "About Us");
        startActivity(intent);

    }

}
