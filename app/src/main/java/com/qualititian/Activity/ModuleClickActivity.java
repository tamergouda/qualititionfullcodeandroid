package com.qualititian.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.qualititian.Adapter.ModuleAdapter;
import com.qualititian.Adapter.TemplateListingAdapter;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModuleClickActivity extends AppCompatActivity {

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;


    @BindView(R.id.ivSort)
    ImageView ivSort;

    @BindView(R.id.rvTempelateListing)
    RecyclerView rvTempelateListing;

    private List<DocResponse> tempelateList=null;
    private TemplateListingAdapter adapter=null;
    private String iD="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_click);

        ButterKnife.bind(this);


        if(getIntent()!=null){

            tv_title.setText(getIntent().getStringExtra("Title"));
            iD=getIntent().getStringExtra("ID");


        }
        setToolbar();

        setupRecyclerView();


        getTempelateDetails();

    }



    public void getTempelateDetails() {
        if(CommonUtilities.isNetworkAvailable(this)){
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> commonModelCall = servicesInterface.getTemplate(iD);
                ServicesConnection.getInstance().enqueueWithoutRetry(commonModelCall, this, true, new Callback<CommonModel>() {
                    @Override
                    public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                        if(response.isSuccessful()){
                            CommonModel commonModel = response.body();
                            if (commonModel.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())){


                                tempelateList.clear();
                                tempelateList.addAll(commonModel.getTemplateListing());
                                adapter.updateList(tempelateList);


                            }else {
                                CommonUtilities.snackBar(ModuleClickActivity.this,commonModel.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonModel> call, Throwable t) {

                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            CommonUtilities.snackBar(this,getString(R.string.no_internet));
        }
    }


    private void setupRecyclerView() {

        tempelateList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this,2);
        rvTempelateListing.setLayoutManager(layoutManager);
        adapter = new TemplateListingAdapter(this, tempelateList);
        rvTempelateListing.setAdapter(adapter);


        adapter.onClickListener(new ModuleAdapter.MyClickInterface() {
            @Override
            public void onClick(DocResponse docResponse) {

                dispatchWebview(docResponse.getTemplate_id());
            }
        });

    }

    private void setToolbar(){

        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        ivSort.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void dispatchAddDoc(){

        Intent intent=new Intent(this,AddDocumentActivity.class);
        startActivity(intent);

    }

    private void dispatchWebview(String templateId) {
        Intent intent = new Intent(this, WebViewChromeClient.class);
        intent.putExtra(ParamEnum.Url.theValue(), "http://mobulous.co.in/qualitian/services/template_view/" + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID)+"/"+templateId);
       // intent.putExtra(ParamEnum.Url.theValue(), "http://mobulous.co.in/qualitian/services/approvedUser1/" + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID)+"/"+templateId);
        intent.putExtra(ParamEnum.Title.theValue(), "Template");
        startActivity(intent);
    }

}
