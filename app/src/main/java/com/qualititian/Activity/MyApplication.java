package com.qualititian.Activity;

import android.app.Application;
import android.content.Context;

import com.qualititian.Utility.NetworkConnectionCheck;

/**
 * Created by mobulous2 on 25/01/17.
 */

public class MyApplication extends Application {

    public static final String TAG = MyApplication.class.getSimpleName();
    private static MyApplication mInstance;
    private static Context context;
    private NetworkConnectionCheck connectionCheck;


    public static boolean networkConnectionCheck()
    {
        if(mInstance.connectionCheck==null)
        {
            mInstance.connectionCheck=new NetworkConnectionCheck(mInstance);
        }
        return mInstance.connectionCheck.isConnect();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        mInstance = this;
        context=getApplicationContext();
        MyApplication.context=this;
       // TypefaceUtil.overrideFont(getApplicationContext(), "Seoge", "fonts/segoeui_0.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
        connectionCheck=new NetworkConnectionCheck(this);

    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onLowMemory(){
        super.onLowMemory();
//        BitmapAjaxCallback.clearCache();
    }

    public static Context getContext() {
        return context;
    }


}