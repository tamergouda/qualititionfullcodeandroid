package com.qualititian.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.qualititian.Adapter.NotificationAdapter;
import com.qualititian.Model.Notify_design;
import com.qualititian.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationScreen extends AppCompatActivity {

    @BindView(R.id.notification_navbtn)ImageView notification_navbtn;

    @BindView(R.id.rvNotification)RecyclerView rvNotification;
    String[] mytimeago= {"15 min,ago","15 min,ago","15 min,ago","15 min,ago","15 min,ago"};
    ArrayList<Notify_design> notify_pojos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_screen);
        ButterKnife.bind(this);

        rvNotification.setHasFixedSize(true);
        rvNotification.setLayoutManager(new LinearLayoutManager(this));
        notify_pojos=new ArrayList<>();
        for (int i=0; i<mytimeago.length; i++)
        {
            Notify_design np=new Notify_design(mytimeago[i]);
            notify_pojos.add(np);
        }
        rvNotification.setAdapter(new NotificationAdapter(this,notify_pojos));
    }


    @OnClick({R.id.notification_navbtn})
    void onCLick(View view){

        switch (view.getId()){

            case R.id.notification_navbtn:
                finish();
                break;
        }

    }
}
