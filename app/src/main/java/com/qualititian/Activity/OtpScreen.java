package com.qualititian.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.Utility.CommonUtilities;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpScreen extends AppCompatActivity {


    @BindView(R.id.otp_resend)TextView otp_resend;
    @BindView(R.id.btnNext)Button btnNext;
    @BindView(R.id.otp_second)EditText otp_second;
    @BindView(R.id.otp_third)EditText otp_third;
    @BindView(R.id.otp_first)EditText otp_first;
    @BindView(R.id.otp_fourth)EditText otp_fourth;

    private Dialog dialog;
    private String Otp="";
    private String Email="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_screen);
        ButterKnife.bind(this);

        if(getIntent()!=null){

            Otp=getIntent().getStringExtra("OTP");
            Email=getIntent().getStringExtra("Email");

        }
        addTextChangeListener();
    }


    @OnClick({R.id.btnNext,R.id.otp_resend})
    void onClick(View view){

        switch (view.getId()){

            case R.id.btnNext:

                if(getValidCode().equalsIgnoreCase(Otp))
                   changePass();
                else
                    CommonUtilities.snackBar(this,"Please enter valid code");


                break;

            case R.id.otp_resend:

                //////to get otp again /////

                resendOtp();

                break;

        }

    }


    private void changePass() {

        dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.change_pass);

        final EditText edNewPass=(EditText)dialog.findViewById(R.id.edNewPass);
        final TextView tvCLose=(TextView)dialog.findViewById(R.id.tvCLose);
        final EditText edConfirmPass=(EditText)dialog.findViewById(R.id.edConfirmPass);
        Button btnChangePass=(Button)dialog.findViewById(R.id.btnChangePass);


        btnChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updatePassword(Email,edNewPass.getText().toString(),edConfirmPass.getText().toString());
            }
        });
        tvCLose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }



    //////////update pass///////

    public void updatePassword(final String Email,final String newPass,final String confrimPass) {
        if(CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.updatePassword(Email,newPass,confrimPass);

                Log.w("DeviceToke", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {

                                        CommonResponse responseBean = bean.getLoginResponse();


                                        Toast.makeText(OtpScreen.this, "Password Changed Succesfully", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(OtpScreen.this, LoginActivity.class);
                                        startActivity(intent);
                                        finishAffinity();



                                    } else {
                                        CommonUtilities.snackBar(OtpScreen.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            CommonUtilities.snackBar(this,getString(R.string.no_internet));

        }
    }

    public String getValidCode(){


        String otp1=otp_first.getText().toString();
        String otp2=otp_second.getText().toString();
        String otp3=otp_third.getText().toString();
        String otp4=otp_fourth.getText().toString();

        String Code=otp1+otp2+otp3+otp4;

        return Code;
    }


    public void resendOtp() {

        if(CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.forgotPassword(Email);

                Log.w("DeviceToke", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {

                                        CommonResponse responseBean = bean.getForgotPassword();

                                        Otp=responseBean.getCode();


                                    } else {
                                        CommonUtilities.snackBar(OtpScreen.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            CommonUtilities.snackBar(this,getString(R.string.no_internet));

        }
    }



   private void addTextChangeListener(){

       otp_first.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {

               if(otp_first.getText().toString().length()==1){

                   otp_first.setCursorVisible(false);
                   otp_second.requestFocus();
                   otp_second.setCursorVisible(true);
               }

           }
       });

       otp_second.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {

               if(otp_second.getText().toString().length()>0){
                   otp_second.setCursorVisible(false);
                   otp_third.requestFocus();
                   otp_third.setCursorVisible(true);
               }else {

                   otp_first.requestFocus();
               }


           }
       });



       otp_third.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {


               if(otp_third.getText().toString().length()>0){
                   otp_third.setCursorVisible(false);
                   otp_fourth.requestFocus();
                   otp_fourth.setCursorVisible(true);
               }else {

                   otp_second.requestFocus();

               }


           }
       });

       otp_fourth.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

           }

           @Override
           public void afterTextChanged(Editable s) {

               if(otp_fourth.getText().toString().length()>0){

                   otp_fourth.requestFocus();
                   otp_fourth.setCursorVisible(true);

               }else {

                   otp_third.requestFocus();

               }


           }
       });
   }

}
