package com.qualititian.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qualititian.R;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;
import com.qualititian.Utility.TakeImage;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PotraitChromClient extends AppCompatActivity {


    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;
    private String path = "";

    @BindView(R.id.webUrl)
    WebView webUrl;

    private static final int CAMERA_REQUEST = 11;
    private  static int RESULT_LOAD_IMAGE = 1;

    private WebSettings webSettings;
    private ValueCallback<Uri[]> mFilePathCallback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_potrait_chrom_client);
        ButterKnife.bind(this);
        if (getIntent().getStringExtra(ParamEnum.Url.theValue()) != null) {

            tv_title.setText(getIntent().getStringExtra(ParamEnum.Title.theValue()));


            setwebView(getIntent().getStringExtra(ParamEnum.Url.theValue()));

          /*  webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.loadUrl(getIntent().getStringExtra("URL"));
*/

           /* webUrl.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    CommonUtilities.dismissLoadingDialog();

                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    CommonUtilities.showLoadingDialog(WebViewActivity.this);

                }

            });
*/

            setSupportActionBar(mainToolbar);
            mainToolbar.setNavigationIcon(R.drawable.um_back);
            getSupportActionBar().setDisplayShowTitleEnabled(false);


            mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });

        }


    }

    //method for setup webivew ////////

    private void setwebView(String url) {


        webSettings = webUrl.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(webSettings.LOAD_CACHE_ELSE_NETWORK);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setAllowFileAccess(true);
        webUrl.getSettings().setBuiltInZoomControls(true);
        webUrl.getSettings().setDisplayZoomControls(false);


        webUrl.setWebChromeClient(new PotraitChromClient.PQChromeClient());
        //if SDK version is greater of 19 then activate hardware acceleration otherwise activate software acceleration
        if (Build.VERSION.SDK_INT >= 19) {
            webUrl.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT < 19) {
            webUrl.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

       // webUrl.setWebViewClient(new PotraitChromClient.PQClient());
        webUrl.loadUrl(url);

    }

    public class PQChromeClient extends WebChromeClient {


        // For Android 5.0
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, FileChooserParams fileChooserParams) {
            // Double check that we don't have any existing callbacks
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePath;

            openBottomSheetBanner();

            return true;

        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

            if(newProgress>100){

                CommonUtilities.showLoadingDialog(PotraitChromClient.this);

            }else {

                CommonUtilities.dismissLoadingDialog();

            }

        }
    }

    public class PQClient extends WebViewClient {
        ProgressDialog progressDialog;

        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            // If url contains mailto link then open Mail Intent
            if (url.contains("mailto:")) {

                // Could be cleverer and use a regex
                //Open links in new browser
                view.getContext().startActivity(
                        new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                // Here we can open new activity

                return true;

            } else {

                // Stay within this webview and load url
                view.loadUrl(url);
                return true;
            }
        }


    }



    private void openBottomSheetBanner() {
        View view = getLayoutInflater().inflate(R.layout.bottomsheet_imagepicker, null);
        final Dialog mBottomSheetDialog = new Dialog(this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();


        view.findViewById(R.id.cameraView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PotraitChromClient.this, TakeImage.class);
                intent.putExtra(ParamEnum.FROM.theValue(), "camera");
                startActivityForResult(intent, CAMERA_REQUEST);
                mBottomSheetDialog.dismiss();
            }
        });

        view.findViewById(R.id.galleryView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PotraitChromClient.this, TakeImage.class);
                intent.putExtra(ParamEnum.FROM.theValue(), "gallery");
                startActivityForResult(intent, RESULT_LOAD_IMAGE);
                mBottomSheetDialog.dismiss();
            }
        });


        view.findViewById(R.id.cancelView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBottomSheetDialog.dismiss();
            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_OK) {

            try {

                path = data.getStringExtra("filePath");
                if (path != null) {
                    File imgFile = new File(data.getStringExtra("filePath"));
                    if (imgFile.exists()) {

                        Uri[] results = new Uri[]{Uri.fromFile(new File(path))};

                        mFilePathCallback.onReceiveValue(results);
                        mFilePathCallback = null;
                    }
                }
            }catch (Exception e){

                e.printStackTrace();
            }


        }
    }

}


