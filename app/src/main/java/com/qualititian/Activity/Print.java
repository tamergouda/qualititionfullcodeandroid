package com.qualititian.Activity;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.qualititian.Adapter.PrintListAdap;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Print extends AppCompatActivity {

    @BindView(R.id.rvPrintDoc)RecyclerView rvPrintDoc;

    @BindView(R.id.mainToolbar)
    android.support.v7.widget.Toolbar mainToolbar;

    private List<DocResponse>docList=null;

    @BindView(R.id.ivSort)
    ImageView ivSort;

    @BindView(R.id.tv_title)
    TextView tv_title;

    private String docId;

    private PrintListAdap adap;
    private BroadcastReceiver onComplete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        ButterKnife.bind(this);

        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        ivSort.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText("Print");


        onComplete   =new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                // your code
                Toast.makeText(ctxt, "Your file has been saved succefully to Download folder", Toast.LENGTH_SHORT).show();
                saveDownlaodList(docId);
            }
        };


        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ////////handle back press////////
        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        docList=new ArrayList<>();
        rvPrintDoc.setLayoutManager(new LinearLayoutManager(this));
        adap=new PrintListAdap(this,docList);
        rvPrintDoc.setAdapter(adap);
        printDocListApi();
        onClick();

    }

    //////////print doc api///////

    public void printDocListApi() {
        if(CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.printDocumentListing(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));

                Log.w("DeviceToken", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {



                                         docList.clear();
                                         docList.addAll(bean.getPrintDocumentListing());
                                         adap.updatePrintDocListing(docList);




                                    } else {
                                        CommonUtilities.snackBar(Print.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            CommonUtilities.snackBar(this,getString(R.string.no_internet));

        }
    }


    private void onClick(){

        adap.onClickListener(new PrintListAdap.MyClickInterface() {
            @Override
            public void onClick(DocResponse docResponse) {


                Intent  intent = new Intent(Print.this, WebViewWithPrint.class);
                intent.putExtra("URL", "http://docs.google.com/gview?embedded=true&url="+docResponse.getFile());
                intent.putExtra("TITLE", "Print");
                startActivity(intent);


            }

            @Override
            public void onDownlaodClick(DocResponse docResponse) {


                docId=docResponse.getId();

                String fileName= docResponse.getFile();
                String extension=fileName.substring(fileName.lastIndexOf("."));



                intitiateDownload(docResponse.getFile(),docResponse.getDocument_title(),extension);

            }
        });


    }

    @OnClick({R.id.ivSort})
     void onClickListener(View view){

        switch (view.getId()){

            case R.id.ivSort:

                Filter();

                break;

        }

    }



    private void intitiateDownload(String uri,String docTitle,String extension){


        Intent intent = getIntent();
        DownloadManager downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
       // Uri Download_Uri = Uri.parse(intent.getStringExtra("Document_href"));
        Uri Download_Uri = Uri.parse(uri);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);

//Restrict the types of networks over which this download may proceed.
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
//Set whether this download may proceed over a roaming connection.
        request.setAllowedOverRoaming(false);
//Set the title of this download, to be displayed in notifications.
        request.setTitle(docTitle+"downloading");
//Set the local destination for the downloaded file to a path within the application's external files directory
        request.setDestinationInExternalFilesDir(this, Environment.DIRECTORY_DOWNLOADS, docTitle+extension);
//Enqueue a new download and same the referenceId
        downloadManager.enqueue(request);

    }


    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(onComplete);
    }

    public void saveDownlaodList(String docId) {
        if(CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.download(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN),docId);

                Log.w("DeviceToken", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {

                                        CommonResponse responseBean = bean.getLoginResponse();


                                    } else {
                                        CommonUtilities.snackBar(Print.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {

            CommonUtilities.snackBar(this,getString(R.string.no_internet));

        }
    }


    /////////filter dialogue////////

    private void Filter() {

        Dialog  dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialogue_print_filter);

        final EditText edDocName=(EditText)dialog.findViewById(R.id.edDocName);
        final EditText edStandardNo=(EditText)dialog.findViewById(R.id.edStandardNo);
        final EditText edDepartment=(EditText)dialog.findViewById(R.id.edDepartment);
        final TextView tvCLose=(TextView)dialog.findViewById(R.id.tvCLose);
        final EditText edManual=(EditText)dialog.findViewById(R.id.edManual);
        Button btnApply=(Button)dialog.findViewById(R.id.btnApply);


        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        tvCLose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        dialog.show();

    }


}
