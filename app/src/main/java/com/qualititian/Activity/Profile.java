package com.qualititian.Activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.CommonResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Profile extends AppCompatActivity {


    @BindView(R.id.notification_navbtn)
    ImageView notification_navbtn;
    @BindView(R.id.edFirst)
    TextInputEditText edFirst;
    @BindView(R.id.edEmailId)
    TextInputEditText edEmailId;
    @BindView(R.id.edPhone)
    TextInputEditText edPhone;
    @BindView(R.id.edCorporateName)
    TextInputEditText edCorporateName;
    @BindView(R.id.edDepartmentName)
    TextInputEditText edDepartmentName;
    @BindView(R.id.edDesignation)
    TextInputEditText edDesignation;
    @BindView(R.id.tvSave)
    TextView tvSave;
    @BindView(R.id.profile_edit)
    ImageView profile_edit;
    @BindView(R.id.clChangePass)
    ConstraintLayout clChangePass;
    @BindView(R.id.edConfirmPass)
    EditText edConfirmPass;
    @BindView(R.id.edNewPass)
    EditText edNewPass;
    @BindView(R.id.edOldPass)
    EditText edOldPass;
    @BindView(R.id.btnChangePass)
    Button btnChangePass;

    private int i = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        tvSave.setVisibility(View.GONE);

        edFirst.setEnabled(false);
        edEmailId.setEnabled(false);
        edPhone.setEnabled(false);
        edCorporateName.setEnabled(false);
        edDepartmentName.setEnabled(false);
        edDesignation.setEnabled(false);


        edOldPass.setVisibility(View.GONE);
        edNewPass.setVisibility(View.GONE);
        edConfirmPass.setVisibility(View.GONE);
        btnChangePass.setVisibility(View.GONE);

        viewProfile();
    }


    @OnClick({R.id.notification_navbtn, R.id.profile_edit,
            R.id.tvSave, R.id.clChangePass,R.id.btnChangePass})
    void onCLick(View view) {

        switch (view.getId()) {

            case R.id.notification_navbtn:
                finish();
                break;

            case R.id.profile_edit:

                tvSave.setVisibility(View.VISIBLE);
                profile_edit.setVisibility(View.GONE);


                edFirst.setEnabled(true);
                edCorporateName.setEnabled(true);
                edDepartmentName.setEnabled(true);
                edDesignation.setEnabled(true);


                break;

            case R.id.tvSave:

                profile_edit.setVisibility(View.VISIBLE);
                tvSave.setVisibility(View.GONE);

                editProfile();

                edFirst.setEnabled(false);
                edEmailId.setEnabled(false);
                edPhone.setEnabled(false);
                edCorporateName.setEnabled(false);
                edDepartmentName.setEnabled(false);
                edDesignation.setEnabled(false);


                break;


            case R.id.clChangePass:


                if (i == 0) {

                    edConfirmPass.setVisibility(View.VISIBLE);
                    edNewPass.setVisibility(View.VISIBLE);
                    edOldPass.setVisibility(View.VISIBLE);
                    btnChangePass.setVisibility(View.VISIBLE);

                    i++;


                } else {

                    edConfirmPass.setVisibility(View.GONE);
                    edNewPass.setVisibility(View.GONE);
                    edOldPass.setVisibility(View.GONE);
                    btnChangePass.setVisibility(View.GONE);
                      i--;
                }


                break;


            case R.id.btnChangePass:

                if(validation())
                    changePass();

                break;
        }

    }

    public void viewProfile() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.viewProfile(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));

                Log.w("DeviceToken", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {

                                        CommonResponse responseBean = bean.getViewProfile();


                                        edFirst.setText(responseBean.getFirst_name());
                                        edEmailId.setText(responseBean.getEmail());
                                        edPhone.setText(responseBean.getMobile());
                                        edCorporateName.setText(responseBean.getType_corprate());
                                        edDepartmentName.setText(responseBean.getDepartment_name());
                                        edDesignation.setText(responseBean.getDesignation());


                                    } else {
                                        CommonUtilities.snackBar(Profile.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(this, getString(R.string.no_internet));

        }
    }


    public void changePass() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.changePassword(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN), edOldPass.getText().toString(), edNewPass.getText().toString());

                Log.w("DeviceToke", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {

                                        CommonResponse responseBean = bean.getViewProfile();

                                        Toast.makeText(Profile.this, "Password change succesfully", Toast.LENGTH_SHORT).show();


                                        edOldPass.setText("");
                                        edNewPass.setText("");
                                        edConfirmPass.setText("");


                                    } else {
                                        CommonUtilities.snackBar(Profile.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(this, getString(R.string.no_internet));

        }
    }


    private boolean validation() {

        if(edOldPass.getText().toString().length()>0){

             if(edNewPass.getText().toString().length()>0){


                 if(edConfirmPass.getText().toString().length()>0){

                     if(edNewPass.getText().toString().equalsIgnoreCase(edConfirmPass.getText().toString())){

                         return true;

                     }else {

                         Toast.makeText(this, "New password and confirm Password do not match", Toast.LENGTH_SHORT).show();


                     }

                 }else {

                     Toast.makeText(this, "Enter confirm password", Toast.LENGTH_SHORT).show();


                 }


             }else {

                 Toast.makeText(this, "Enter new password", Toast.LENGTH_SHORT).show();

             }


        }else {

            Toast.makeText(this, "Enter old password", Toast.LENGTH_SHORT).show();

        }

        return false;
    }

    public void editProfile() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.editProfile(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN),
                        edFirst.getText().toString(),edEmailId.getText().toString(),edPhone.getText().toString(),edDesignation.getText().toString(),edCorporateName.getText().toString(),edDepartmentName.getText().toString());

                Log.w("DeviceToke", "6697974");


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {

                                        CommonResponse responseBean = bean.getViewProfile();

                                         CommonUtilities.snackBar(Profile.this,"Profile updated successfully");

                                    } else {
                                        CommonUtilities.snackBar(Profile.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(this, getString(R.string.no_internet));

        }
    }


}
