package com.qualititian.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.qualititian.R;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;

import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_TIMER = 3000;
    private static final int PERMISSION_REQUEST_CODE = 12;
    @RequiresApi(api = Build.VERSION_CODES.M)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ////////////hide toolbar/////////////
        ApplyTransparentStatusBar();

       // getDeviceToken();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        if (checkPermissions()) {

            try {
                final Timer timer = new Timer();
                TimerTask task = new TimerTask() {

                    @Override
                    public void run() {

                        if(SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.ISLOGIN).equalsIgnoreCase("Login")) {
                            Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();
                        }
                        else {
                            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(mainIntent);
                            finish();
                           // getDeviceToken();

                        }
                    }
                };
                timer.schedule(task, 2000);
            } catch (Exception ex) {

            }

        }
    }


    void ApplyTransparentStatusBar()
    {
        Window window = getWindow();
        if (window != null)
        {
            View decor = window.getDecorView();
            if (decor != null)
                decor.setSystemUiVisibility(ResolveTransparentStatusBarFlag());
        }
    }

    int ResolveTransparentStatusBarFlag()
    {
        String[] libs = getPackageManager().getSystemSharedLibraryNames();
        String reflect = null;

        if (libs == null)
            return 0;

        for (String lib : libs)
        {
            if (lib.equals("touchwiz"))
                reflect = "SYSTEM_UI_FLAG_TRANSPARENT_BACKGROUND";
            else if (lib.startsWith("com.sonyericsson.navigationbar"))
                reflect = "SYSTEM_UI_FLAG_TRANSPARENT";
        }

        if (reflect == null)
            return 0;

        try
        {
            Field field = View.class.getField(reflect);
            if (field.getType() == Integer.TYPE)
                return field.getInt(null);
        }
        catch (Exception e)
        {
        }

        return 0;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS,Manifest.permission.CALL_PHONE,Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            switch (requestCode) {
                case PERMISSION_REQUEST_CODE:
                    if(SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.ISLOGIN).equalsIgnoreCase("Login")) {
                        Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }
                    else {
                        Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(mainIntent);
                        finish();

                    }
                    break;
            }
        } else {
            Toast.makeText(this, "Please accept permissions due to security purpose", Toast.LENGTH_SHORT).show();
            checkPermissions();
        }
    }


    private void getDeviceToken() {

        final Thread thread = new Thread() {
            @Override
            public void run() {
                Log.e(">>>>>>>>>>>>>>", "thred IS  running");
                try {
                    if (SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.DEVICETOKEN).isEmpty()) {
                        String token = FirebaseInstanceId.getInstance().getToken();
//                        String token = Settings.Secure.getString(getApplicationContext().getContentResolver(),
//                                Settings.Secure.ANDROID_ID);
                        Log.e("Generated Device Token", "-->" + token);
                        if (token == null) {
                            getDeviceToken();
                        } else {

                            SharedPreferenceWriter.getInstance(SplashActivity.this).writeStringValue(SPreferenceKey.DEVICETOKEN,token);
                            //mPreference.writeStringValue(SharedPreferenceKey.DEVICE_TOKEN, token);
                        }
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                super.run();
            }
        };
        thread.start();

    }


}
