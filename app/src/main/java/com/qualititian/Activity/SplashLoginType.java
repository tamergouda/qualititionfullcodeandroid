package com.qualititian.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.qualititian.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashLoginType extends AppCompatActivity {

    @BindView(R.id.btnGuest)Button btnGuest;
    @BindView(R.id.btnClient)Button btnClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_login_type);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnGuest,R.id.btnClient})
    void setClick(View view){

        Intent intent;

        switch (view.getId()){

            case R.id.btnGuest:

                intent=new Intent(this,GuestActivity.class);
                startActivity(intent);

                break;

            case R.id.btnClient:

                intent=new Intent(this,ClientActivity.class);
                startActivity(intent);


                break;

        }

    }
}
