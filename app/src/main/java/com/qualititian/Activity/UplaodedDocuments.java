package com.qualititian.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.qualititian.Adapter.UplaodedDocAdap;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UplaodedDocuments extends AppCompatActivity {

    @BindView(R.id.rvUplaodedDoc)
    RecyclerView rvUplaodedDoc;

    private List<DocResponse> docList = null;
    private UplaodedDocAdap adapter;

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.ivSort)
    ImageView ivSort;


    @BindView(R.id.tv_title)
    TextView tv_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uplaoded_documents);
        ButterKnife.bind(this);

        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        ivSort.setVisibility(View.GONE);
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText("Uplaoded Documents");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        docList = new ArrayList<>();
        rvUplaodedDoc.setLayoutManager(new LinearLayoutManager(this));
        adapter = new UplaodedDocAdap(this, docList);
        rvUplaodedDoc.setAdapter(adapter);

        docListinApi();

    }

    //////////document listing api///////

    public void docListinApi() {
        if (CommonUtilities.isNetworkAvailable(this)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.documentListing(SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));

                Log.w("Token---------", SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.TOKEN));


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        this,
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {


                                        docList.clear();
                                        docList.addAll(bean.getDocResponseList());
                                        adapter.updateDocListing(docList);


                                    } else {
                                        CommonUtilities.snackBar(UplaodedDocuments.this, bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(this, getString(R.string.no_internet));

        }
    }

}
