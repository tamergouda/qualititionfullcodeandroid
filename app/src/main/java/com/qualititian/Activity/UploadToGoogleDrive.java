package com.qualititian.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.MetadataChangeSet;
import com.qualititian.R;
import com.qualititian.Utility.FileUtil;
import com.qualititian.Utility.ParamEnum;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UploadToGoogleDrive extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks{


    private static final String TAG = "drive-quickstart";
    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;
    private static final int REQUEST_CODE_CREATOR = 2;
    private static final int REQUEST_CODE_RESOLUTION = 3;

    private GoogleApiClient mGoogleApiClient=null;
    private Bitmap mBitmapToSave;
    String path = "";
    private static final int REQUEST_CODE_SIGN_IN = 0;


    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;


    @BindView(R.id.ivSort)
    ImageView ivSort;

    @BindView(R.id.btnUplaodToDrive)
    Button btnUplaodToDrive;

    private static final int REQUEST_CODE_OPEN=5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uplaod_to_google_drive);
        ButterKnife.bind(this);


        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        ivSort.setVisibility(View.GONE);
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(ParamEnum.ADD_DOC.theValue());


        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    @OnClick({R.id.btnUplaodToDrive})
    void Onclick(View view){

        switch (view.getId()){

            case R.id.btnUplaodToDrive:

//                Intent pickIntent = new Intent(Intent.ACTION_GET_CONTENT);
//                pickIntent.setType("*/*");
//                startActivityForResult(pickIntent,REQUEST_CODE_OPEN);

                String[] mimeTypes =
                        {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                "text/plain",
                                "application/pdf",
                                "application/zip"};
                Intent intent = new Intent();
                intent.setType("application/* | text/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent,
                        "Upload to Dropbox"), REQUEST_CODE_OPEN);


                break;

        }


    }

    private void saveFileToDrive(File file) {
        // Start by creating a new contents, and setting a callback.
        Log.i(TAG, "Creating new contents.");
        final Bitmap image = mBitmapToSave;
        Drive.DriveApi.newDriveContents(mGoogleApiClient)
                .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {



                    @Override
                    public void onResult(DriveApi.DriveContentsResult result) {
                        // If the operation was not successful, we cannot do anything
                        // and must
                        // fail.
                        if (!result.getStatus().isSuccess()) {
                            Log.i(TAG, "Failed to create new contents.");
                            return;
                        }

                        OutputStream outputStream = result.getDriveContents().getOutputStream();

                        //uplaodImageToDrive(result,image);


                        /////////////meta data for image////////////////
                        // Create an intent for the file chooser, and start it.
                      /*  IntentSender intentSender = Drive.DriveApi
                                .newCreateFileActivityBuilder()
                                .setInitialMetadata(metadataChangeSet)
                                .setInitialDriveContents(result.getDriveContents())
                                .build(mGoogleApiClient);*/


                        Toast.makeText(UploadToGoogleDrive.this, "Uploading to drive", Toast.LENGTH_LONG).show();
                        //final File theFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/xtests/tehfile.txt"); //>>>>>> WHAT FILE ?
                        try {
                            FileInputStream fileInputStream = new FileInputStream(file);
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                                outputStream.write(buffer, 0, bytesRead);
                            }
                        } catch (IOException e1) {
                            Log.i(TAG, "Unable to write file contents.");
                        }


                        //MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(file.getName()).setMimeType("text/plain").setStarred(false).build();
                        MetadataChangeSet changeSet = new MetadataChangeSet.Builder().setTitle(file.getName()).setMimeType("application/pdf").setStarred(false).build();


                        /////////////meta data for file////////////////
                        // Create an intent for the file chooser, and start it.
                        IntentSender intentSender = Drive.DriveApi
                                .newCreateFileActivityBuilder()
                                .setInitialMetadata(changeSet)
                                .setInitialDriveContents(result.getDriveContents())
                                .build(mGoogleApiClient);



                        try {
                            startIntentSenderForResult(
                                    intentSender, REQUEST_CODE_CREATOR, null, 0, 0, 0);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "Failed to launch file chooser.");
                        }
                    }
                });
    }



    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null) {
            // Create the API client and bind it to an instance variable.
            // We use this instance as the callback for connection and connection
            // failures.
            // Since no account name is passed, the user is prompted to choose.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        // Connect the client. Once connected, the camera is launched.
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {

        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }

        super.onPause();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_CAPTURE_IMAGE:
                // Called after a photo has been taken.
                if (resultCode == Activity.RESULT_OK) {
                    // Store the image data as a bitmap for writing later.
                    mBitmapToSave = (Bitmap) data.getExtras().get("data");
                }
                break;
            case REQUEST_CODE_CREATOR:
                // Called after a file is saved to Drive.
                if (resultCode == RESULT_OK) {
                    Log.i(TAG, "Image successfully saved.");
                    mBitmapToSave = null;

                    Toast.makeText(this, "File Uplaod Succesfully", Toast.LENGTH_SHORT).show();
                    // Just start the camera again for another photo.
                    /*startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                            REQUEST_CODE_CAPTURE_IMAGE);*/
                }
                break;

            case REQUEST_CODE_OPEN:

                if(resultCode==RESULT_OK){

                    final Uri uri = data.getData();

                    path = FileUtil.getPath(this, uri);

                    File file=new File(path);

                    saveFileToDrive(file);

                }

                break;
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Called whenever the API client fails to connect.
        Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GoogleApiAvailability.getInstance().getErrorDialog(this, result.getErrorCode(), 0).show();
            return;
        }
        // The failure has a resolution. Resolve it.
        // Called typically when the app is not yet authorized, and an
        // authorization
        // dialog is displayed to the user.
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Exception while starting resolution activity", e);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "API client connected.");

    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended");
    }

    public void uplaodImageToDrive(DriveApi.DriveContentsResult result, Bitmap image){


        // Otherwise, we can write our data to the new contents.
        Log.i(TAG, "New contents created.");
        // Get an output stream for the contents.
        OutputStream outputStream = result.getDriveContents().getOutputStream();
        // Write the bitmap data from it.
        ByteArrayOutputStream bitmapStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, bitmapStream);

        try {
            outputStream.write(bitmapStream.toByteArray());
        } catch (IOException e1) {
            Log.i(TAG, "Unable to write file contents.");
        }
        // Create the initial metadata - MIME type and title.
        // Note that the user will be able to change the title later.
        MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
                .setMimeType("image/jpeg").setTitle("Android Photo.png").build();


    }
}
