package com.qualititian.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.qualititian.R;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.ParamEnum;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserManagement extends AppCompatActivity {

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.clApproved)
    ConstraintLayout clApproved;
    @BindView(R.id.clPendingUser)
    ConstraintLayout clPendingUser;
    @BindView(R.id.clAddUser)
    ConstraintLayout clAddUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_management);
        ButterKnife.bind(this);

        setSupportActionBar(mainToolbar);
        mainToolbar.setNavigationIcon(R.drawable.um_back);
        mainToolbar.setVisibility(View.VISIBLE);
        tv_title.setVisibility(View.VISIBLE);

        tv_title.setText(ParamEnum.USER_MANA.theValue());
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }


    @OnClick({R.id.clApproved, R.id.clPendingUser, R.id.clAddUser})
    void onClick(View view) {

        switch (view.getId()) {

            case R.id.clApproved:

                dispatchApprovedWebview();

                break;

            case R.id.clPendingUser:

                dispatchPendignUseWebView();

                break;

            case R.id.clAddUser:

                dispatchAddUserWebview();

                break;


        }

    }


    private void dispatchApprovedWebview() {
        Intent intent = new Intent(this, WebViewChromeClient.class);
        intent.putExtra(ParamEnum.Url.theValue(), "http://mobulous.co.in/qualitian/services/approvedUser1/" + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID));
        intent.putExtra(ParamEnum.Title.theValue(), "Approved");
        startActivity(intent);
    }

    private void dispatchAddUserWebview() {
        Intent intent = new Intent(this, WebViewChromeClient.class);
        intent.putExtra(ParamEnum.Url.theValue(), "http://mobulous.co.in/qualitian/services/addMember/" + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID));
        intent.putExtra(ParamEnum.Title.theValue(), "Add Member");
        startActivity(intent);
    }

    private void dispatchPendignUseWebView() {
        Intent intent = new Intent(this, WebViewChromeClient.class);
        intent.putExtra(ParamEnum.Url.theValue(), "http://mobulous.co.in/qualitian/services/pendingUser1/" + SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.USERID));
        intent.putExtra(ParamEnum.Title.theValue(), "Pending User");
        startActivity(intent);
    }
}
