package com.qualititian.Activity;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.qualititian.R;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends AppCompatActivity {


    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.webUrl)
    WebView webUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        if (getIntent().getStringExtra(ParamEnum.Url.theValue()) != null) {

            tv_title.setText(getIntent().getStringExtra(ParamEnum.Title.theValue()));

            webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.loadUrl(getIntent().getStringExtra(ParamEnum.Url.theValue()));


            webUrl.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    CommonUtilities.dismissLoadingDialog();

                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    CommonUtilities.showLoadingDialog(WebViewActivity.this);

                }

            });


            setSupportActionBar(mainToolbar);
            mainToolbar.setNavigationIcon(R.drawable.um_back);
            getSupportActionBar().setDisplayShowTitleEnabled(false);


            mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });

        }


    }
}
