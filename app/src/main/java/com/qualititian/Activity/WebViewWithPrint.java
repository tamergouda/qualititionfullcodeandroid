package com.qualititian.Activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import com.qualititian.R;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewWithPrint extends AppCompatActivity {

    @BindView(R.id.mainToolbar)
    Toolbar mainToolbar;

    @BindView(R.id.tv_title)
    TextView tv_title;

    @BindView(R.id.webUrl)
    WebView webUrl;

    @BindView(R.id.webViewPrint)
    FloatingActionButton webViewPrint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_with_print);
        ButterKnife.bind(this);
        if (getIntent().getStringExtra("URL") != null) {

            tv_title.setText(getIntent().getStringExtra("TITLE"));

            webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.getSettings().setPluginState(WebSettings.PluginState.ON);
            webUrl.getSettings().setAllowFileAccess(true);
            webUrl.loadUrl(getIntent().getStringExtra(ParamEnum.Url.theValue()));


            webUrl.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    CommonUtilities.dismissLoadingDialog();

                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    CommonUtilities.showLoadingDialog(WebViewWithPrint.this);

                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    super.onReceivedError(view, request, error);
                    CommonUtilities.dismissLoadingDialog();
                }
            });


            setSupportActionBar(mainToolbar);
            mainToolbar.setNavigationIcon(R.drawable.um_back);
            getSupportActionBar().setDisplayShowTitleEnabled(false);


            mainToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });

        }



        webViewPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                createWebPrintJob(webUrl);

            }
        });

    }






    //create a function to create the print job
    private void createWebPrintJob(WebView webView) {

        //create object of print manager in your device
        PrintManager printManager = (PrintManager) this.getSystemService(Context.PRINT_SERVICE);

        //create object of print adapter
        PrintDocumentAdapter printAdapter = webView.createPrintDocumentAdapter();

        //provide name to your newly generated pdf file
        String jobName = getString(R.string.app_name) + " Print Test";

        //open print dialog
        printManager.print(jobName, printAdapter, new PrintAttributes.Builder().build());
    }



}
