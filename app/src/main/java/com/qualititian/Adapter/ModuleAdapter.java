package com.qualititian.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ModuleAdapter extends RecyclerView.Adapter<ModuleAdapter.MyModuleHolder> {

    private LayoutInflater inflater;
    private List<DocResponse> docList;
    private Context context;
    private MyClickInterface listener;

    public ModuleAdapter(Context context, List<DocResponse> docList){

        this.context=context;
        this.docList=docList;

    }


    public void updatePrintDocListing(List<DocResponse> docList){

        this.docList=docList;
        notifyDataSetChanged();

    }

    @Override
    public ModuleAdapter.MyModuleHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.layout_module, parent, false);

        return new ModuleAdapter.MyModuleHolder(view);

    }

    @Override
    public void onBindViewHolder(ModuleAdapter.MyModuleHolder holder, final int position) {

        holder.tvModuleName.setText(docList.get(position).getTemplateType());


    }



    @Override
    public int getItemCount() {
        return docList.size();
    }

    public class MyModuleHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvModuleName)
        TextView tvModuleName;


        public MyModuleHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            tvModuleName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onClick(docList.get(getAdapterPosition()));

                }
            });



        }

    }

    public interface MyClickInterface{

        void onClick(DocResponse docResponse);

    }


    public void onClickListener(MyClickInterface listener){

        this.listener=listener;

    }

}
