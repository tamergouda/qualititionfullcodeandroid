package com.qualititian.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahipal Singh on 18,October,2018
 * mahisingh1@outlook.com
 */
public class MyUploadedDocAdap extends RecyclerView.Adapter<MyUploadedDocAdap.MyUploadedDocHolder> {

    private LayoutInflater inflater;
    private List<DocResponse> docList;
    private Context context;


    public MyUploadedDocAdap(Context context, List<DocResponse> docList){

        this.context=context;
        this.docList=docList;

    }


    public void updateDocListing(List<DocResponse> docList){

        this.docList=docList;
        notifyDataSetChanged();

    }

    @Override
    public MyUploadedDocHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.mydocument_layout, parent, false);

        return new MyUploadedDocHolder(view);

    }

    @Override
    public void onBindViewHolder(MyUploadedDocHolder holder, final int position) {

        holder.tvTitle.setText(docList.get(position).getDocument_title());


        if(!docList.get(position).getHospital_logo().equalsIgnoreCase(""))
            Glide.with(context).load(docList.get(position).getHospital_logo()).into(holder.ivThumbImage);
        else
            Glide.with(context).load(R.drawable.default_image).into(holder.ivThumbImage);




    }

    public void removeAllocatePlan(int position){

        this.docList.remove(position);
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return docList.size();
    }

    public class MyUploadedDocHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.ivThumbImage)
        ImageView ivThumbImage;



        public MyUploadedDocHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);


        }

    }



}
