package com.qualititian.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qualititian.Model.Notify_design;
import com.qualititian.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mobulous06 on 30/5/18.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    List<Notify_design> pushResponseList;
    Context context;
    private LayoutInflater inflater;

    public NotificationAdapter(Context context, List<Notify_design> pushResponseList) {
        this.context = context;
        this.pushResponseList = pushResponseList;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());

        View view=inflater.inflate(R.layout.notification_layout, parent, false);

        return new NotificationAdapter.MyViewHolder(view);
    }

    public void UpdateList(List<Notify_design> pushResponseList){
        this.pushResponseList= pushResponseList;
        notifyDataSetChanged();

    }

    @Override
    public void onBindViewHolder(NotificationAdapter.MyViewHolder holder, int position) {


       holder.tvTime.setText(pushResponseList.get(position).getMtimeago());
       // holder.tv_notif_content.setText(pushResponseList.get(position).getMessage());

        // Glide.with(context).load(group_data.get(position).getMain_img()).override(600,600).into(holder.binding.mainImg);
        //holder.binding.tvName.setText(gyms_data.get(position).getUser_name());
/*

        if(!pushResponseList.get(position).getImage().equalsIgnoreCase(""))
            Glide.with(context).load(pushResponseList.get(position).getImage()).into(holder.ivUserProfile);
        else
            Glide.with(context).load(R.drawable.profile_signup).into(holder.ivUserProfile);
*/

    }

    @Override
    public int getItemCount() {
        return pushResponseList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivProfile)ImageView ivProfile;
        @BindView(R.id.tvMessage)TextView tvMessage;
        @BindView(R.id.tvTime)TextView tvTime;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);



        }
    }
}
