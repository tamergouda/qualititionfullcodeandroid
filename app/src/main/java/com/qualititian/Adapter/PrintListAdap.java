package com.qualititian.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahipal Singh on 16,October,2018
 * mahisingh1@outlook.com
 */


public class PrintListAdap extends RecyclerView.Adapter<PrintListAdap.MyDocListHolder> {

    private LayoutInflater inflater;
    private List<DocResponse> docList;
    private Context context;
    private MyClickInterface listener;

    public PrintListAdap(Context context, List<DocResponse> docList){

        this.context=context;
        this.docList=docList;

    }


    public void updatePrintDocListing(List<DocResponse> docList){

        this.docList=docList;
        notifyDataSetChanged();

    }

    @Override
    public MyDocListHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.print_layout, parent, false);

        return new MyDocListHolder(view);

    }

    @Override
    public void onBindViewHolder(MyDocListHolder holder, final int position) {

        holder.tvDocTitle.setText(docList.get(position).getDocument_title());


        if(!docList.get(position).getHospital_logo().equalsIgnoreCase(""))
            Glide.with(context).load(docList.get(position).getHospital_logo()).into(holder.ivDocImage);
        else
            Glide.with(context).load(R.drawable.default_image).into(holder.ivDocImage);



    }



    @Override
    public int getItemCount() {
        return docList.size();
    }

    public class MyDocListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDocTitle)
        TextView tvDocTitle;

        @BindView(R.id.ivDocImage)
        ImageView ivDocImage;

        @BindView(R.id.ivDown)
        ImageView ivDown;

        @BindView(R.id.btnPrint)
        Button btnPrint;



        public MyDocListHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            btnPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onClick(docList.get(getAdapterPosition()));

                }
            });

            ivDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onDownlaodClick(docList.get(getAdapterPosition()));

                }
            });

        }

    }

    public interface MyClickInterface{

        void onClick(DocResponse docResponse);
        void onDownlaodClick(DocResponse docResponse);

    }


    public void onClickListener(MyClickInterface listener){

        this.listener=listener;

    }

}
