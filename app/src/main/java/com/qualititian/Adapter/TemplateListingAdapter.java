package com.qualititian.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TemplateListingAdapter extends RecyclerView.Adapter<TemplateListingAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private List<DocResponse> docList;
    private Context context;
    private ModuleAdapter.MyClickInterface listener;

    public TemplateListingAdapter(Context context, List<DocResponse> docList){

        this.context=context;
        this.docList=docList;

    }

    public void updateList(List<DocResponse> docList){

        this.docList=docList;
        notifyDataSetChanged();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.mydocument_layout, parent, false);

        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvTitle.setText(docList.get(position).getTemplate_name());


        if(!docList.get(position).getFile().equalsIgnoreCase(""))
            Glide.with(context).load(docList.get(position).getFile()).into(holder.ivThumbImage);
        else
            Glide.with(context).load(R.drawable.default_image).into(holder.ivThumbImage);



    }



    @Override
    public int getItemCount() {
        return docList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        TextView tvTitle;

        @BindView(R.id.ivThumbImage)
        ImageView ivThumbImage;


        @BindView(R.id.llMain)
        LinearLayout llMain;



        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

            llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    listener.onClick(docList.get(getAdapterPosition()));

                }
            });



        }

    }

    public interface MyClickInterface{

        void onClick(DocResponse docResponse);

    }


    public void onClickListener(ModuleAdapter.MyClickInterface listener){

        this.listener=listener;

    }

}
