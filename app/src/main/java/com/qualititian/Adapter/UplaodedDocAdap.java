package com.qualititian.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mahipal Singh on 12,September,2018
 * mahisingh1@outlook.com
 */
public class UplaodedDocAdap extends RecyclerView.Adapter<UplaodedDocAdap.MyBookDateHolder> {

    private LayoutInflater inflater;
    private List<DocResponse> docList;
    private Context context;


    public UplaodedDocAdap(Context context, List<DocResponse> docList){

        this.context=context;
        this.docList=docList;

    }


    public void updateDocListing(List<DocResponse> docList){

        this.docList=docList;
        notifyDataSetChanged();

    }

    @Override
    public MyBookDateHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (inflater == null)
            inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.uploaded_documents, parent, false);

        return new MyBookDateHolder(view);

    }

    @Override
    public void onBindViewHolder(MyBookDateHolder holder, final int position) {

        holder.tvDocTitle.setText(docList.get(position).getDocument_title());


        if(!docList.get(position).getHospital_logo().equalsIgnoreCase(""))
            Glide.with(context).load(docList.get(position).getHospital_logo()).into(holder.ivDocThumb);
        else
            Glide.with(context).load(R.drawable.default_image).into(holder.ivDocThumb);




    }

    public void removeAllocatePlan(int position){

        this.docList.remove(position);
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return docList.size();
    }

    public class MyBookDateHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDocTitle)
        TextView tvDocTitle;

        @BindView(R.id.ivDocThumb)
        ImageView ivDocThumb;

      

        public MyBookDateHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

           
        }

    }

   

}
