package com.qualititian.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.qualititian.Activity.Analytics;
import com.qualititian.Activity.Backup;
import com.qualititian.Activity.DocumentsModuleTab;
import com.qualititian.Activity.Print;
import com.qualititian.Activity.Profile;
import com.qualititian.Activity.UserManagement;
import com.qualititian.R;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeFragment extends Fragment {


    @BindView(R.id.cvProfile)ConstraintLayout cvProfile;
    @BindView(R.id.clPrint)ConstraintLayout clPrint;
    @BindView(R.id.clAnalytics)ConstraintLayout clAnalytics;
    @BindView(R.id.clUser)ConstraintLayout clUser;
    @BindView(R.id.Card_backup)CardView Card_backup;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);


        if(SharedPreferenceWriter.getInstance(getContext()).getString(SPreferenceKey.USER_TYPE).equalsIgnoreCase("51")) {
            cvProfile.setEnabled(false);
            clUser.setEnabled(false);
        }else {
            cvProfile.setEnabled(true);
            clUser.setEnabled(true);
        }

       return view;

    }

    @OnClick({R.id.Card_backup,R.id.Card_doc,
            R.id.cvProfile,R.id.clPrint
            ,R.id.clAnalytics,R.id.clUser})
    void onClick(View view){

        Intent intent;

        switch (view.getId()){

            case R.id.cvProfile:

            intent=new Intent(getContext(), Profile.class);
            startActivity(intent);


            break;

            case R.id.clPrint:

            intent=new Intent(getContext(), Print.class);
            startActivity(intent);


            break;

            case R.id.clAnalytics:

            intent=new Intent(getContext(), Analytics.class);
            startActivity(intent);


            break;

            case R.id.clUser:

            intent=new Intent(getContext(), UserManagement.class);
            startActivity(intent);


            break;

            case R.id.Card_doc:

            intent=new Intent(getContext(), DocumentsModuleTab.class);
            startActivity(intent);


            break;



            case R.id.Card_backup:

            if(SharedPreferenceWriter.getInstance(getContext()).getString(SPreferenceKey.ROLE).equalsIgnoreCase("Owner")) {
                intent = new Intent(getContext(), Backup.class);
                startActivity(intent);
            }else {

                CommonUtilities.snackBar(getActivity(),"You are not authorized for back up");

            }

            break;


        }

    }

}
