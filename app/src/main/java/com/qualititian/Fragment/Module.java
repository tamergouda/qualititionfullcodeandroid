package com.qualititian.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qualititian.Activity.ModuleClickActivity;
import com.qualititian.Adapter.ModuleAdapter;
import com.qualititian.Adapter.PrintListAdap;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;
import com.qualititian.Utility.ParamEnum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Module extends Fragment {

//    @BindView(R.id.cvHealth)
//    CardView cvHealth;
//
//    @BindView(R.id.cvFmcg)
//    CardView cvFmcg;
//
//    @BindView(R.id.cvIndustrial)
//    CardView cvIndustrial;
//
//    @BindView(R.id.cvImport)
//    CardView cvImport;


    @BindView(R.id.rvModule)
    RecyclerView rvModule;

    private ModuleAdapter adapter = null;
    private List<DocResponse> moduleList = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_module, container, false);
        ButterKnife.bind(this, view);
        setupRecyclerView();

        return view;
    }


    public void getmoduleListing(Activity contex) {
        if (CommonUtilities.isNetworkAvailable(contex)) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> commonModelCall = servicesInterface.getmoduleListing();
                ServicesConnection.getInstance().enqueueWithoutRetry(commonModelCall, contex, true, new Callback<CommonModel>() {
                    @Override
                    public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                        if (response.isSuccessful()) {
                            CommonModel commonModel = response.body();
                            if (commonModel.getStatus().equalsIgnoreCase(ParamEnum.Success.theValue())) {


                                moduleList.clear();
                                moduleList.addAll(commonModel.getTemplateType());
                                adapter.updatePrintDocListing(moduleList);


                            } else {
                                CommonUtilities.snackBar(contex, commonModel.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CommonModel> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            CommonUtilities.snackBar(contex, getString(R.string.no_internet));
        }
    }

    private void setupRecyclerView() {

        moduleList = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvModule.setLayoutManager(layoutManager);
        adapter = new ModuleAdapter(getActivity(), moduleList);
        rvModule.setAdapter(adapter);


        adapter.onClickListener(new ModuleAdapter.MyClickInterface() {
            @Override
            public void onClick(DocResponse docResponse) {

                dispatchTemplateAct(docResponse.getId());
            }
        });

    }

    private void dispatchTemplateAct(String id) {

        Intent intent = new Intent(getContext(), ModuleClickActivity.class);
        intent.putExtra("Title", "Tempelate");
        intent.putExtra("ID", id);
        startActivity(intent);

    }
}
