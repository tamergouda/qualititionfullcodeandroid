package com.qualititian.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qualititian.Activity.UplaodedDocuments;
import com.qualititian.Adapter.MyUploadedDocAdap;
import com.qualititian.Adapter.UplaodedDocAdap;
import com.qualititian.Model.CommonModel;
import com.qualititian.Model.DocResponse;
import com.qualititian.R;
import com.qualititian.Retrofit.ServicesConnection;
import com.qualititian.Retrofit.ServicesInterface;
import com.qualititian.SharedPrefrence.SPreferenceKey;
import com.qualititian.SharedPrefrence.SharedPreferenceWriter;
import com.qualititian.Utility.CommonUtilities;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyDocuments extends Fragment {

    private List<DocResponse> docList = null;


    private MyUploadedDocAdap adapter;

    @BindView(R.id.rvMyUplaodDoc)
     RecyclerView rvMyUplaodDoc;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_documents, container, false);
        ButterKnife.bind(this,view);

        docList = new ArrayList<>();
        rvMyUplaodDoc.setLayoutManager(new GridLayoutManager(getActivity(),2));
        adapter = new MyUploadedDocAdap(getActivity(), docList);
        rvMyUplaodDoc.setAdapter(adapter);

        docListinApi();

        return view;
    }

    //////////document listing api///////

    public void docListinApi() {
        if (CommonUtilities.isNetworkAvailable(getActivity())) {
            try {
                ServicesInterface servicesInterface = ServicesConnection.getInstance().createService();
                Call<CommonModel> loginCall = servicesInterface.documentListing(SharedPreferenceWriter.getInstance(getActivity()).getString(SPreferenceKey.TOKEN));

                Log.w("Token---------", SharedPreferenceWriter.getInstance(getContext()).getString(SPreferenceKey.TOKEN));


                ServicesConnection.getInstance().enqueueWithoutRetry(
                        loginCall,
                        getActivity(),
                        true,
                        new Callback<CommonModel>() {
                            @Override
                            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {
                                if (response.isSuccessful()) {
                                    CommonModel bean = ((CommonModel) response.body());
                                    if (bean.getStatus().equalsIgnoreCase("SUCCESS")) {


                                        docList.clear();
                                        docList.addAll(bean.getDocResponseList());
                                        adapter.updateDocListing(docList);


                                    } else {
                                        CommonUtilities.snackBar(getActivity(), bean.getMessage());

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CommonModel> call, Throwable t) {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            CommonUtilities.snackBar(getActivity(), getString(R.string.no_internet));

        }
    }


}
