package com.qualititian.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mahipal Singh on 25,June,2018
 * mahisingh1@outlook.com
 */
public class CommonModel {

    @SerializedName("documentListing")
    List<DocResponse>docResponseList;

    @SerializedName("dwonloadListing")
    List<DocResponse>dwonloadListing;

    @SerializedName("templateType")
    List<DocResponse>templateType;

    public List<DocResponse> getTemplateListing() {
        return templateListing;
    }

    public void setTemplateListing(List<DocResponse> templateListing) {
        this.templateListing = templateListing;
    }

    @SerializedName("templateListing")
    List<DocResponse>templateListing;

    public List<DocResponse> getTemplateType() {
        return templateType;
    }

    public void setTemplateType(List<DocResponse> templateType) {
        this.templateType = templateType;
    }

    @SerializedName("printDocumentListing")
    List<DocResponse>printDocumentListing;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    @SerializedName("requestKey")
    private String requestKey;


    @SerializedName("login")
    private CommonResponse loginResponse;

    @SerializedName("clientSignup")
    private CommonResponse clientSignup;


    @SerializedName("guestSignup")
    private CommonResponse guestSignup;
    @SerializedName("viewProfile")
    private CommonResponse viewProfile;
    @SerializedName("forgotPassword")
    private CommonResponse forgotPassword;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {

        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRequestKey() {
        return requestKey;
    }

    public void setRequestKey(String requestKey) {
        this.requestKey = requestKey;
    }

    public CommonResponse getLoginResponse() {
        return loginResponse;
    }

    public void setLoginResponse(CommonResponse loginResponse) {
        this.loginResponse = loginResponse;
    }

    public List<DocResponse> getDocResponseList() {
        return docResponseList;
    }

    public void setDocResponseList(List<DocResponse> docResponseList) {
        this.docResponseList = docResponseList;
    }

    public List<DocResponse> getDwonloadListing() {
        return dwonloadListing;
    }

    public void setDwonloadListing(List<DocResponse> dwonloadListing) {
        this.dwonloadListing = dwonloadListing;
    }

    public List<DocResponse> getPrintDocumentListing() {
        return printDocumentListing;
    }

    public void setPrintDocumentListing(List<DocResponse> printDocumentListing) {
        this.printDocumentListing = printDocumentListing;
    }

    public CommonResponse getGuestSignup() {
        return guestSignup;
    }

    public void setGuestSignup(CommonResponse guestSignup) {
        this.guestSignup = guestSignup;
    }

    public CommonResponse getForgotPassword() {
        return forgotPassword;
    }

    public void setForgotPassword(CommonResponse forgotPassword) {
        this.forgotPassword = forgotPassword;
    }

    public CommonResponse getClientSignup() {
        return clientSignup;
    }

    public void setClientSignup(CommonResponse clientSignup) {
        this.clientSignup = clientSignup;
    }

    public CommonResponse getViewProfile() {
        return viewProfile;
    }

    public void setViewProfile(CommonResponse viewProfile) {
        this.viewProfile = viewProfile;
    }


}
