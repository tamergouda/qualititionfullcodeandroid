package com.qualititian.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mahipal Singh on 12,October,2018
 * mahisingh1@outlook.com
 */
public class DocResponse {


    @SerializedName("document_title")
    private String document_title;

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    @SerializedName("template_id")
    private String template_id;

    @SerializedName("template_name")
    private String template_name;

    public String getTemplate_name() {
        return template_name;
    }

    public void setTemplate_name(String template_name) {
        this.template_name = template_name;
    }

    @SerializedName("hospital_logo")
    private String hospital_logo;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @SerializedName("file")
    private String file;

    @SerializedName("id")
    private String id;

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    @SerializedName("template_type")
    private String templateType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospital_logo() {
        return hospital_logo;
    }

    public void setHospital_logo(String hospital_logo) {
        this.hospital_logo = hospital_logo;
    }

    public String getDocument_title() {
        return document_title;
    }

    public void setDocument_title(String document_title) {
        this.document_title = document_title;
    }



}
