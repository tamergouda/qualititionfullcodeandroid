package com.qualititian.Retrofit;


import com.qualititian.Model.CommonModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

/**
 * Created by mahipal on 26/Sep/18.
 */

public interface ServicesInterface {


    ////////////login paramerters////////////

    @FormUrlEncoded
    @POST("login")
    Call<CommonModel> login(@Field("email") String email,
                            @Field("devicetype") String devicetype,
                            @Field("devicetoken") String devicetoken,
                            @Field("password") String password);


    @FormUrlEncoded
    @POST("forgotPassword")
    Call<CommonModel> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("userFilter")
    Call<CommonModel> userFilter(@Field("token") String token,
                                 @Field("lat") String lat,
                                 @Field("log") String log,
                                 @Field("facilities") String facilities,
                                 @Field("type") String type,
                                 @Field("rating") String rating);


    @FormUrlEncoded
    @POST("updatePassword")
    Call<CommonModel> updatePassword(@Field("email") String email,
                                     @Field("new_password") String new_password,
                                     @Field("confirm_password") String confirm_password);


    @FormUrlEncoded
    @POST("viewProfile")
    Call<CommonModel> viewProfile(@Field("token") String token);

    @FormUrlEncoded
    @POST("printDocumentListing")
    Call<CommonModel> printDocumentListing(@Field("token") String token);


    @FormUrlEncoded
    @POST("editProfile")
    Call<CommonModel> editProfile(@Field("token") String token,
                                  @Field("first_name") String first_name,
                                  @Field("email") String email,
                                  @Field("mobile") String mobile,
                                  @Field("designation") String designation,
                                  @Field("type_corprate") String type_corprate,
                                  @Field("department_name") String department_name);


    @FormUrlEncoded
    @POST("changePassword")
    Call<CommonModel> changePassword(@Field("token") String token,
                                     @Field("oldpassword") String oldpassword,
                                     @Field("newpassword") String newpassword);


    @FormUrlEncoded
    @POST("download")
    Call<CommonModel> download(@Field("token") String token,
                                  @Field("doc_id") String doc_id);

    @FormUrlEncoded
    @POST("gymTrainerDetails")
    Call<CommonModel> gymTrainerDetails(@Field("token") String token,
                                        @Field("trainer_id") String trainer_id);

    @FormUrlEncoded
    @POST("gymTrainerListing")
    Call<CommonModel> gymTrainerListing(@Field("token") String token,
                                        @Field("provider_id") String provider_id);


    @FormUrlEncoded
    @POST("notification")
    Call<CommonModel> notification(@Field("token") String token,
                                   @Field("status") String status);

    @FormUrlEncoded
    @POST("support")
    Call<CommonModel> support(@Field("token") String token,
                              @Field("query_data") String query_data);

    @FormUrlEncoded
    @POST("notificationStatus")
    Call<CommonModel> notificationStatus(@Field("token") String token);

    @Multipart
    @POST("addDocument")
    Call<CommonModel> addDocument(@PartMap Map<String, RequestBody> partMapData,
                                  @Part MultipartBody.Part file,
                                  @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("changePassword2")
    Call<CommonModel> changePassword2(@Field("token") String token,
                                      @Field("old_password") String old_password,
                                      @Field("new_password") String new_password,
                                      @Field("confirm_password") String confirm_password);

    @FormUrlEncoded
    @POST("bookingDetails")
    Call<CommonModel> bookingDetails(@Field("token") String token,
                                     @Field("class_id") String class_id,
                                     @Field("trainer_id") String trainer_id,
                                     @Field("provider_id") String provider_id);


    @FormUrlEncoded
    @POST("userPushHistory")
    Call<CommonModel> userPushHistory(@Field("token") String token);


    @FormUrlEncoded
    @POST("myBookingDetails")
    Call<CommonModel> myBookingDetails(@Field("token") String token,
                                       @Field("lat") String lat,
                                       @Field("log") String log,
                                       @Field("class_id") String class_id,
                                       @Field("provider_id") String provider_id,
                                       @Field("time") String time,
                                       @Field("date") String date,
                                       @Field("book_id") String book_id);


    @FormUrlEncoded
    @POST("forgotPassword")
    Call<CommonModel> forgotPassword(@Field("phone_number") String phone_number,
                                     @Field("new_password") String new_password,
                                     @Field("confirm_password") String confirm_password);


    @FormUrlEncoded
    @POST("cancalBooking")
    Call<CommonModel> cancalBooking(@Field("token") String token,
                                    @Field("book_id") String book_id,
                                    @Field("time") String time,
                                    @Field("reason") String reason);

    @FormUrlEncoded
    @POST("userProfile")
    Call<CommonModel> userProfile(@Field("token") String token);

    @FormUrlEncoded
    @POST("myWallet")
    Call<CommonModel> myWallet(@Field("token") String token);


    @FormUrlEncoded
    @POST("reviews")
    Call<CommonModel> reviews(@Field("token") String token,
                              @Field("type") String type,
                              @Field("comment") String comment,
                              @Field("rating") String rating,
                              @Field("type_id") String type_id);


    @FormUrlEncoded
    @POST("sendCode")
    Call<CommonModel> sendotp(@Field("country_code") String country_code,
                              @Field("phone_number") String phone_number,
                              @Field("type") String type);


    @FormUrlEncoded
    @POST("checkSocial")
    Call<CommonModel> checkSocial(@Field("social_id") String social_id,
                                  @Field("full_name") String full_name,
                                  @Field("email") String email,
                                  @Field("social_type") String social_type,
                                  @Field("phone_number") String phone_number,
                                  @Field("url") String url,
                                  @Field("devicetype") String devicetype,
                                  @Field("devicetoken") String devicetoken);


    @FormUrlEncoded
    @POST("logout")
    Call<CommonModel> logout(@Field("token") String token);


    @FormUrlEncoded
    @POST("providerClassListing")
    Call<CommonModel> providerClassListing(@Field("token") String token);


    @FormUrlEncoded
    @POST("backDelete")
    Call<CommonModel> backDelete(@Field("token") String token);

    @FormUrlEncoded
    @POST("mycancalBooking")
    Call<CommonModel> mycancalBooking(@Field("token") String token);


    @FormUrlEncoded
    @POST("paymentHistory")
    Call<CommonModel> paymentHistory(@Field("token") String token);


    @FormUrlEncoded
    @POST("documentListing")
    Call<CommonModel> documentListing(@Field("token") String token);

   @FormUrlEncoded
    @POST("dwonloadListing")
    Call<CommonModel> dwonloadListing(@Field("token") String token);


    @FormUrlEncoded
    @POST("groupClassListing")
    Call<CommonModel> groupClassListing(@Field("token") String token,
                                        @Field("provider_id") String provider_id);


    @FormUrlEncoded
    @POST("passListing")
    Call<CommonModel> passListing(@Field("token") String token,
                                  @Field("provider_id") String provider_id);

    @FormUrlEncoded
    @POST("myPassListing")
    Call<CommonModel> myPassListing(@Field("token") String token);


    @Multipart
    @POST("clientSignup")
    Call<CommonModel> signUpClient(@PartMap Map<String, RequestBody> part,
                                   @Part MultipartBody.Part file);

    @Multipart
    @POST("clientSignup")
    Call<CommonModel> signUpClient(@PartMap Map<String, RequestBody> part);


    @Multipart
    @POST("guestSignup")
    Call<CommonModel> signUpGuest(@PartMap Map<String, RequestBody> part,
                                  @Part MultipartBody.Part file);

    @Multipart
    @POST("guestSignup")
    Call<CommonModel> signUpGuest(@PartMap Map<String, RequestBody> part);


    @FormUrlEncoded
    @POST("calender")
    Call<CommonModel> calender(@Field("token") String token);


    @FormUrlEncoded
    @POST("datetime")
    Call<CommonModel> datetime(@Field("token") String token,
                               @Field("date") String date,
                               @Field("provider_id") String provider_id,
                               @Field("class_id") String class_id,
                               @Field("trainer_id") String trainer_id);


    @FormUrlEncoded
    @POST("temptime")
    Call<CommonModel> temptime(@Field("token") String token,
                               @Field("date") String date,
                               @Field("time") String time,
                               @Field("status") String status);


    @FormUrlEncoded
    @POST("daytime")
    Call<CommonModel> daytime(@Field("token") String token,
                              @Field("date") String date);

    @GET("TemplateType")
    Call<CommonModel> getmoduleListing();

    @FormUrlEncoded
    @POST("templateListing")
    Call<CommonModel> getTemplate(@Field("template_type") String id);

}






