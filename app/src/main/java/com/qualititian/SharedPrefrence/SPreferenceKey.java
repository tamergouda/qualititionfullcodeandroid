package com.qualititian.SharedPrefrence;


public class SPreferenceKey {

	public static final String PASSWORD = "password";
	public static final String TOKEN = "token";
	public static final String DROPBOX_TOKEN = "dropbox_toke";
	public static final String DEVICETOKEN = "DEVICETOKEN";
	public static final String NAME = "name";
	public static final String PHONENUMBER = "phonenumber";
	public static final String USERNAMEMAIL = "username_mail";
	public static final String ISLOGIN = "login";
	public static final String USERID = "userid";
	public static final String USER_TYPE = "user_type";
	public static final String USERINTEREST = "userinterest";
	public static final String USERSTATE = "USERSTATE";
	public static final String LOGIN_EMAIL_PHONE= "login_email_phone";
	public static final String THUMBIMAGE= "thumb_image";
	public static final String ROLE = "role";
	public static final String ISREMEBER = "isRemeber";

    public static String LAT="lat";
	public static String LON="lon";
	public static String AMOUNT;


}
