package com.qualititian.Utility;

/**
 * Created by Mahipal on 2/11/18.
 */
public enum ParamEnum {

    USER_MANA("User Managment"),
    Url("URL"),
    Success("SUCCESS"),
    FROM("from"),
    ADD_DOC("Add Documents"),
    Title("TITLE");

    private final String value;

    ParamEnum(String value) {
        this.value=value;
    }

    ParamEnum(){
     this.value=null;
    }

    public String theValue() {
        return this.value;
    }

}