/*
package com.qualititian.Utility;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

*/
/**
 * Created by Mahipal Singh on 19,October,2018
 * mahisingh1@outlook.com
 *//*

public class UploadFileToDropbox extends AsyncTask<Void, Void, Boolean> {

    private DropboxAPI<?> dropbox;
    private String path;
    private Context context;

    private DropboxAPI<AndroidAuthSession> mDBApi;


    public UploadFileToDropbox(Context context, DropboxAPI<?> dropbox,
                               String path) {
        this.context = context.getApplicationContext();
        this.dropbox = dropbox;
        this.path = path;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        final File tempDir = context.getCacheDir();
        File tempFile;
        FileWriter fr;
        File file = new File(path);
        FileInputStream inputStream = null;

        DropboxAPI.Entry response = null;

        try {

            inputStream = new FileInputStream(file);


            try {
               dropbox.putFile("/my_file.pdf", inputStream,
                        file.length(), null, null);
                //Log.i("DbExampleLog", "The uploaded file's rev is: " + response.rev);
            } catch (DropboxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }

            return true;
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (result) {
            Toast.makeText(context, "File Uploaded Sucesfully!",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Failed to upload file", Toast.LENGTH_LONG)
                    .show();
        }
    }

*/
/*
    public void uploadFile() {
        writeFileContent(file_path);
        File file = new File(file_path);
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }


        try {
            response = dropbox.putFile("/my_file.txt", inputStream,
                    file.length(), null, null);
            Log.i("DbExampleLog", "The uploaded file's rev is: " + response.rev);
        } catch (DropboxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

    }
*//*

}
*/
